﻿using UnityEngine;

public class LightTrigger : MonoBehaviour
{
	void Start()
	{
		SpotLightObject.active = false; //at start light will remain off
	}


	public Transform SpotLightObject;

	//SpotLightObject.light.disabled;

	void OnTriggerEnter(Collider other)
	{
		SpotLightObject.active = true;
		audio.Play();

		Destroy(this);
	}
}