﻿	using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class PersistentData : MonoBehaviour 
{
	#region Constants
	public const int MainMenuLevelId = 0;
	public const int FirstEsmondLevelId = 7;
	public const int FirstAnnaLevelId = 1;
	public const int EndGameLevelId = 13;
	public const int SharedLevel = 10;
	#endregion

	#region Variables
	[SerializeField] GUIText m_InteractionText;
	[SerializeField] Inventory m_PlayerInventory;

	[SerializeField] GameObject m_InGameUI;
	[SerializeField] GUIText m_ThrowableText;

	public static PersistentData instance = null;


	#endregion

	#region Properties
	public Inventory PlayerInventory { get { return m_PlayerInventory; } }
	#endregion

	#region Functions
	// Use this for initialization
	void Awake()
	{
		transform.position = Vector3.zero;
		Init();

		/*if (Screen.fullScreen)
		{
			Screen.showCursor = false;
		}*/
	}

	void Init()
	{
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(gameObject);
			if (Application.loadedLevel != MainMenuLevelId && Application.loadedLevel != EndGameLevelId)
			{
				m_InGameUI.SetActive(true);
			}
		}
		else
		{
			Destroy(gameObject);
		}
	}



	public void SetInteractionText(string text)
	{
		m_InteractionText.text = text;
	}

	public void ShowInteractionText(bool show)
	{
		m_InteractionText.gameObject.SetActive(show);
	}

	public void ShowInGameUI(bool show)
	{
		m_InGameUI.SetActive(show);
	}

	public void Reset()
	{
		PlayerInventory.Reset();
		SetThrowableCount(0);
	}

	public void SetThrowableCount(int count)
	{
		if (m_ThrowableText != null)
		{
			m_ThrowableText.text = "x" + count.ToString();
		}
	}

	void OnLevelWasLoaded(int level)
	{
		Debug.Log("OnLevelLoaded");
		ShowInteractionText(false);
		SetInteractionText("");

		if (m_InGameUI != null)
		{
			m_InGameUI.SetActive(level != MainMenuLevelId && level != EndGameLevelId);
		}

		if (level == MainMenuLevelId)
		{
			Reset();
			audio.Stop();
		}
		else if (level == EndGameLevelId)
		{
			audio.Stop();
		}
		else if (level == SharedLevel)
		{
			Note[] sharedNotes = FindObjectsOfType<Note>();
			for (int i = 0; i < sharedNotes.Length; i++)
			{
				if (m_PlayerInventory.GetNoteData(sharedNotes[i].Data.m_ID) != null)
				{
					// they've already collected this note
					Destroy(sharedNotes[i].gameObject);
				}
			}
		}
		else
		{
			audio.Play();
		}
	}

	public int DetermineGameEnding(ref bool savedKid)
	{
		int index = 0;
		Inventory inventory = PersistentData.instance.PlayerInventory;
		savedKid = inventory.KidEscaped;
		
		if (inventory.getResearchNoteCount() >= 4)
		{
			// good ending
			index = 0;
		}
		else
		{
			// bad ending
			index += 4;
		}
		
		if (inventory.CollectedMedicine || inventory.CollectedComputerFile)
		{
			// possitive ending
		}
		else
		{
			// negative ending
			index += 2;
		}

		if (GameController.instance.DeathCount > 0)
		{
			index += 1;
		}
		
		return index;
	}
	#endregion
}
