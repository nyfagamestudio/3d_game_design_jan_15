﻿using UnityEngine;
using System.Collections;

public class Utils
{
	public static void TurnToFace(GameObject turner, GameObject target, float turnSpeed)
	{
		Quaternion targetRot = Quaternion.LookRotation(target.transform.position - turner.transform.position);
		float speed = Mathf.Min (turnSpeed * Time.deltaTime, 1);
		turner.transform.rotation = Quaternion.Lerp (turner.transform.rotation, targetRot, speed);
	}

	public static void TurnToFace(GameObject turner, Vector3 gazePosition, Vector3 targetPosition, float turnSpeed)
	{
		Quaternion targetRot = Quaternion.LookRotation(targetPosition - gazePosition);
		float speed = Mathf.Min (turnSpeed * Time.deltaTime, 1);
		turner.transform.rotation = Quaternion.Lerp (turner.transform.rotation, targetRot, speed);
	}
}
