﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NoteWidget : MonoBehaviour {

	#region Variables
	[SerializeField] Toggle m_Toggle;
	[SerializeField] Text m_NoteName;
	[SerializeField] int m_NoteId;
	[SerializeField] Image m_NoteBackground;
	#endregion

	#region Properties
	public int NoteId { get { return m_NoteId; } }
	#endregion

	#region Functions
	void Start()
	{
		//UnlockNote(false);
	}


	public void UnlockNote(bool unlock)
	{
		m_Toggle.interactable = unlock;
		if (unlock)
		{
			m_NoteName.text = "Note #" + (m_NoteId + 1);
		}
		else
		{
			m_NoteName.text = "???";
			m_Toggle.isOn = false;
			ShowNoteBackground(false);
		}

		//Debug.Log("m_NoteName.text: " + m_NoteName.text);
	}

	public void ShowNoteBackground(bool show)
	{
		m_NoteBackground.gameObject.SetActive(show);
	}
	#endregion
}
