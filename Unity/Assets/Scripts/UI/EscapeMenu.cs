﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EscapeMenu : MonoBehaviour {

	public Image m_EsmondPortrait; 
	InventoryMenu m_InventoryMenu;
	ControlsMenu m_ControlsMenu;

	// Use this for initialization
	void Start () {
	
	}

	void Update()
	{
		m_EsmondPortrait.gameObject.SetActive(Application.loadedLevel >= PersistentData.FirstEsmondLevelId);
	}

	public void Setup(InventoryMenu inventoryMenu, ControlsMenu controlsMenu)
	{
		m_InventoryMenu = inventoryMenu;
		m_ControlsMenu = controlsMenu;
	}
	
	public void OnMainMenuButtonPress ()
	{
		Application.LoadLevel ("MainMenu");
		gameObject.SetActive(false);
	}

	public void OnNotesButtonPress ()
	{
		m_InventoryMenu.gameObject.SetActive(true);
		gameObject.SetActive(false);
	}

	public void OnControlsButtonPress ()
	{
		Debug.Log("ControlsMenu");
		m_ControlsMenu.gameObject.SetActive(true);
		gameObject.SetActive(false);
	}

}
