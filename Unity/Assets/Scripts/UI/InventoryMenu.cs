﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InventoryMenu : MonoBehaviour 
{
	#region Variables
	[SerializeField] Text m_NoteText;
	[SerializeField] NoteWidget[] m_NoteWidgets;
	NoteWidget m_SelectedNote;
	//Button m_SelectedNote;
	#endregion

	#region Functions
	public void SelectNote(int noteId)
	{
		//m_SelectedNote = noteButton;

	}

	public void UnlockNote(int noteIndex)
	{
		m_NoteWidgets[noteIndex].UnlockNote(true);
	}

	public void LockAllNotes()
	{
		for (int i = 0; i < m_NoteWidgets.Length; i++)
		{
			m_NoteWidgets[i].UnlockNote(false);
		}
	}

	public void SelectNote(NoteWidget noteWidget)
	{
		if (m_SelectedNote != null)
		{
			m_SelectedNote.ShowNoteBackground(false);
		}

		m_SelectedNote = noteWidget;

		Note.NoteData noteData = PersistentData.instance.PlayerInventory.GetNoteData(noteWidget.NoteId);
		noteWidget.ShowNoteBackground(true);
		if (noteData != null)
		{
			SetNoteText(noteData.m_Text);
		}
		else
		{
			SetNoteText("");
		}
	}

	void SetNoteText(string noteText)
	{
		m_NoteText.text = noteText;
	}

	void HighlightNoteButton(Button noteButton)
	{
		//noteButton.GetComponentInChildren<Text>().color = Color.yellow;
	}

	public void Close()
	{
		gameObject.SetActive(false);
	}
	#endregion
}
