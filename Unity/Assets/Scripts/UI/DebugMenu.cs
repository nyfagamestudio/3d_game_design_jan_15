﻿using UnityEngine;
using System.Collections;

public class DebugMenu : MonoBehaviour {

	bool m_ShowGui = false;
	// Use this for initialization
	void Start () {
	
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.C))
		{
			m_ShowGui = !m_ShowGui;
		}
	}
	
	void OnGUI()
	{
		if (!m_ShowGui)
		{
			return;
		}

		if (GUILayout.Button("LV1")) { LoadLevel(1); } 
		if (GUILayout.Button("LV2")) { LoadLevel(2); } 
		if (GUILayout.Button("LV3")) { LoadLevel(3); } 
		if (GUILayout.Button("LV4")) { LoadLevel(4); } 
		if (GUILayout.Button("LV5")) { LoadLevel(5); } 
		if (GUILayout.Button("LV6")) { LoadLevel(6); } 
		if (GUILayout.Button("LV7")) { LoadLevel(7); } 
		if (GUILayout.Button("LV8")) { LoadLevel(8); } 
		if (GUILayout.Button("LV9")) { LoadLevel(9); } 
		if (GUILayout.Button("LV10")) { LoadLevel(10); } 
		if (GUILayout.Button("LV11")) { LoadLevel(11); } 
		if (GUILayout.Button("LV12")) { LoadLevel(12); } 
	}

	void LoadLevel(int levelId)
	{
		Application.LoadLevel(levelId);
	}
}
