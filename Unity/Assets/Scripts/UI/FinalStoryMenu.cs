﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FinalStoryMenu : MonoBehaviour
{
	#region Variables
	[SerializeField] Text[] m_Text;
	[SerializeField] Text m_SavedKidText;
	[SerializeField] Text m_DidNotSaveKidText;
	[SerializeField] Text m_DisplayedText;
	#endregion

	#region Functions
	void Start()
	{
		bool savedKid = false;
		int index = PersistentData.instance.DetermineGameEnding(ref savedKid);
		index = Mathf.Clamp(index, 0, m_Text.Length - 1);
		Debug.Log("Ending: " + m_Text[index].name);
		m_DisplayedText.text = m_Text[index].text;

		m_DisplayedText.text += "\n\n";
		m_DisplayedText.text += savedKid ? m_SavedKidText.text : m_DidNotSaveKidText.text;
	}

	public void ReturnToMainMenu()
	{
		Application.LoadLevel(PersistentData.MainMenuLevelId);
	}
	#endregion
}
