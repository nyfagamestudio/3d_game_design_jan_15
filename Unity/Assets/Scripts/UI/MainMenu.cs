﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenu : MonoBehaviour {

	[SerializeField] GameObject m_CreditsPanel;
	[SerializeField] GameObject m_ControlsPanel;

	// Use this for initialization
	void Start () {
	
	}

	public void OnStartButtonClicked()
	{
		Debug.Log("Start button clicked");
		Application.LoadLevel ("LV1");
	}

	public void OnExitButtonClicked ()
	{
		Debug.Log ("Exit Button Clicked");
		Application.Quit();
	}

	public void OnControlsButtonClicked ()
	{
		Debug.Log ("Controls Button Clicked");
		// Turns Control Scheme on/off

		m_ControlsPanel.gameObject.SetActive(true);
	}

	public void CloseControls()
	{
		m_ControlsPanel.gameObject.SetActive(false);
	}

	public void OnCreditsButtonClicked ()
	{
		Debug.Log ("Credit Button Clicked");
		m_CreditsPanel.gameObject.SetActive(true);
	}

	public void CloseCredits()
	{
		m_CreditsPanel.gameObject.SetActive(false);
	}
}
