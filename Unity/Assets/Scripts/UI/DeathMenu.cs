﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DeathMenu : MonoBehaviour 
{
	public Button m_ContinueButton;
	public Text m_ReasonForDying;
	public AudioClip EsmondScream;
	public AudioClip AnnaScream;

	// Use this for initialization
	void Start () {
	
	}

	public void ShowContinueButton(bool show)
	{
		Debug.Log("ShowContinueButton: " + show);
		m_ContinueButton.gameObject.SetActive(show);
	}

	public void OnMainMenuButtonPress () {
		gameObject.SetActive(false);
		Debug.Log("OnMainMenuButtonPress");
		Application.LoadLevel(PersistentData.MainMenuLevelId);
	}

	public void OnContinueButtonPress () {
		gameObject.SetActive(false);
		m_ContinueButton.gameObject.SetActive(false);
		Application.LoadLevel(PersistentData.FirstEsmondLevelId);
	}

	public void OnRestartButtonPress () {
		gameObject.SetActive(false);
		Debug.Log("OnRestartButtonPress");
		Application.LoadLevel(PersistentData.FirstAnnaLevelId);
	}

	public void PlayEsmondScream () {
		gameObject.SetActive (true);
		audio.clip = EsmondScream;
		audio.Play ();
		Debug.Log ("PlayEsmondScream");
	}

	public void PlayAnnaScream () {
		gameObject.SetActive (true);
		audio.clip = AnnaScream;
		audio.Play ();

		Debug.Log ("PlayAnnaScream");
	}
}


