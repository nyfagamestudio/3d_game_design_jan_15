﻿using UnityEngine;
using System.Collections;

public class Rotation : MonoBehaviour {

	[SerializeField] Light redLight;

	private Vector3 redTemp;

	[SerializeField] int speed; 

	// Update is called once per frame
	void Update () {
	
		redTemp.y += speed * Time.deltaTime;

		redLight.transform.eulerAngles = redTemp;

	}
}
