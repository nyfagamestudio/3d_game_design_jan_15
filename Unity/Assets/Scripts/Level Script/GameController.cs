﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class GameController : MonoBehaviour
{
	#region Constants
	const int NumLives = 2;
	#endregion

	#region Variables
	public static GameController instance;

	[SerializeField] DeathTimer m_DeathTimer;
	int m_DeathCount = 0;

	public InventoryMenu m_InventoryMenu;
	public EscapeMenu m_EscapeMenu;
	public DeathMenu m_DeathMenu;
	public ControlsMenu m_ControlsMenu;
	public Canvas m_Canvas;
	public EventSystem m_EventSystem;

	MouseLook[] m_PlayerMouseLook;
	#endregion

	#region Properties
	public int DeathCount { get { Debug.Log("m_DeathCount: " + m_DeathCount); return m_DeathCount; } }
	#endregion

	#region Functions
    void Awake()
    {
		Init();
    }

	void Init()
	{
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(gameObject);
			CreateMenus();
		}
		else
		{
			Destroy(gameObject);
		}
	}

	void CreateMenus()
	{
		m_Canvas = Instantiate(m_Canvas) as Canvas;
		DontDestroyOnLoad(m_Canvas.gameObject);
		m_EventSystem = Instantiate(m_EventSystem) as EventSystem;
		DontDestroyOnLoad(m_EventSystem.gameObject);
		
		m_InventoryMenu = Instantiate(m_InventoryMenu) as InventoryMenu;
		m_EscapeMenu = Instantiate(m_EscapeMenu) as EscapeMenu;
		m_DeathMenu = Instantiate(m_DeathMenu) as DeathMenu;
		m_ControlsMenu = Instantiate(m_ControlsMenu) as ControlsMenu;
		
		SetupMenu(m_InventoryMenu.gameObject, m_Canvas.transform, false);
		SetupMenu(m_EscapeMenu.gameObject, m_Canvas.transform, false);
		SetupMenu(m_DeathMenu.gameObject, m_Canvas.transform, false);
		SetupMenu(m_ControlsMenu.gameObject, m_Canvas.transform, false);
		
		m_EscapeMenu.Setup(m_InventoryMenu, m_ControlsMenu);

		PersistentData.instance.PlayerInventory.SetInventoryMenu(m_InventoryMenu);
	}

	
	void SetupMenu(GameObject menu, Transform parent, bool active)
	{
		menu.transform.SetParent(parent, false);
		menu.SetActive(active);
		DontDestroyOnLoad(menu);
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape) && Application.loadedLevel != PersistentData.MainMenuLevelId /*main menu*/)
		{
			if (!IsNonEscapeMenuOpen())
			{
				m_EscapeMenu.gameObject.SetActive(!m_EscapeMenu.gameObject.activeSelf);
			}
		}

		if (m_PlayerMouseLook != null)
		{
			for (int i = 0; i < m_PlayerMouseLook.Length; i++)
			{
				m_PlayerMouseLook[i].enabled = !IsMenuOpen();
			}
		}

		PersistentData.instance.ShowInGameUI(!IsMenuOpen());
		Screen.showCursor = IsMenuOpen() || Application.loadedLevelName == "MainMenu" || Application.loadedLevel == PersistentData.EndGameLevelId;
	}
		


	public void OnPlayerDeath(string reason)
	{
		// show ui with reason of failure
	
		m_DeathCount += 1;
		Debug.Log("m_DeathCount: " + m_DeathCount);

		if (Application.loadedLevel >= PersistentData.FirstEsmondLevelId) {
			m_DeathCount = 2;
			m_DeathMenu.PlayEsmondScream ();
			//audio.Play (PlayEsmondScream);
		} 
		else 
		{

			m_DeathMenu.PlayAnnaScream();
			//audio.Play (PlayAnnaScream);
		}


		m_DeathTimer.ShowText = false;
		m_DeathMenu.m_ReasonForDying.text = reason;
		m_DeathMenu.ShowContinueButton(m_DeathCount < 2);
		m_DeathMenu.gameObject.SetActive(true);
		



		if (m_DeathCount == NumLives)
		{
			// both characters have died, game over
			ResetGameData();
		}
		else
		{
			// clear the number of keys
			PersistentData.instance.PlayerInventory.ResetKeys();
		}

	}

	void ResetGameData()
	{
		m_DeathCount = 0;
		//m_DeathMenu.m_ContinueButton.gameObject.SetActive(true);
		PersistentData.instance.Reset();
	}

	public bool IsMenuOpen()
	{
		return m_InventoryMenu.gameObject.activeSelf || m_EscapeMenu.gameObject.activeSelf
			|| m_DeathMenu.gameObject.activeSelf || m_ControlsMenu.gameObject.activeSelf;
	}

	bool IsNonEscapeMenuOpen()
	{
		return m_InventoryMenu.gameObject.activeSelf || m_DeathMenu.gameObject.activeSelf || m_ControlsMenu.gameObject.activeSelf;
	}

	void OnLevelWasLoaded(int level)
	{
		m_PlayerMouseLook = FindObjectsOfType<MouseLook>();
	}


	#endregion
}
