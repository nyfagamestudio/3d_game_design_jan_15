﻿using UnityEngine;
using System.Collections;

public class LightFlicker : MonoBehaviour 
{

	public float interval = 0.3F;
	public float minFlkrRange = 3;
	public float maxFlkrRange = 8;
	public float timeLeft;
	public Light lt;


	void Start() 
	{
		lt = GetComponent<Light>();
		lt.type = LightType.Point;
		timeLeft = interval;
	}

	void Update() 
	{
		timeLeft -= Time.deltaTime;
		if (timeLeft < 0.0F) 
		{
			timeLeft = interval;
			lt.intensity = Random.Range(minFlkrRange, maxFlkrRange);
		}

	}
}
