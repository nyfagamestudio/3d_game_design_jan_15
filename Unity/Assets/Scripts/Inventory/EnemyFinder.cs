﻿using UnityEngine;
using System.Collections;

public class EnemyFinder : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnTriggerEnter(Collider target)
	{
		Debug.Log(" Something in hearing radius !"+ target.name);
		if(target.tag == "Enemy")
			{
			Debug.Log(" enemy in hearing radius !"+target.name);
				target.GetComponent<Enemy>().SetState(Enemy.EnemyState.Distracted);	
			}
	}
}
