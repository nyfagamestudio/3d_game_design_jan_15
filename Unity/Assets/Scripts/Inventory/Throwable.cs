﻿using UnityEngine;
using System.Collections;

public class Throwable : MonoBehaviour {

	// Use this for initialization
	#region variables
	//public Vector3 PointOfImpact;
	[SerializeField] float m_DistractionDistance = 10;
	#endregion


	void OnCollisionEnter(Collision collision) 
	{
		Enemy[] enemies = FindObjectsOfType<Enemy>();

		if (enemies != null)
		{
			foreach (Enemy enemy in enemies)
			{
				if (Vector3.Distance(transform.position, enemy.transform.position) <= m_DistractionDistance)
				{
					enemy.Distract(transform.position);
				}
			}
		}

		/*GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
		sphere.AddComponent<EnemyFinder>();
		sphere.transform.position = PointOfImpact;
		sphere.transform.localScale = new Vector3(50,50,50);
		sphere.GetComponent<SphereCollider>().isTrigger=true;*/

		audio.Play();
		enabled = false;
		GetComponentInChildren<MeshRenderer>().enabled = false;
		Destroy(gameObject, audio.clip.length);
	}
}
