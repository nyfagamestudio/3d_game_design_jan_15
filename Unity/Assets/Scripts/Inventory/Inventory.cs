﻿using UnityEngine;
using System.Collections;

public class Inventory : MonoBehaviour {

	#region Constants
	const int MaxNumNotes = 13;
	#endregion

	#region variables
	// Use this for initialization
	int BreakableObjectCount;
	int SpecialNoteCount;
	int ResearchNoteCount;
	int RegularNoteCount;
	int KeyCount;
	bool m_CollectedMedicine;
	bool m_CollectedComputerFile;
	bool m_SavedKid;
	bool m_KidEscaped;
	Note.NoteData[] m_CollectedNotes = new Note.NoteData[MaxNumNotes];
	InventoryMenu m_InventoryMenu;
	#endregion

	#region properties
	//public int NumBreakableObjects { get { return BreakableObject; } }
	public bool CollectedMedicine
	{
		get { return m_CollectedMedicine; }
		set { m_CollectedMedicine = value; }
	}

	public bool CollectedComputerFile
	{
		get { return m_CollectedComputerFile; }
		set { m_CollectedComputerFile = value; }
	}

	public bool SavedKid
	{
		get { return m_SavedKid; }
		set { m_SavedKid = value; }
	}

	public bool KidEscaped
	{
		get { return m_KidEscaped; }
		set { m_KidEscaped = value; }
	}
	#endregion

	#region functions

	public void SetInventoryMenu(InventoryMenu inventoryMenu)
	{
		Debug.Log("SetInventoryMenu");
		m_InventoryMenu = inventoryMenu;
	}

	public int getSpecialNoteCount()
	{
		return(SpecialNoteCount);
	}
	public int getResearchNoteCount()
	{
		return(ResearchNoteCount);
	}
	public int getRegularNoteCount()
	{
		return(RegularNoteCount);
	}
	public int getBreakableObjectCount()
	{
		return(BreakableObjectCount);
	}
	public int getKeyCount()
	{
		return(KeyCount);
	}

	public Note.NoteData GetNoteData(int noteIndex)
	{
		Note.NoteData noteData = null;
		if (noteIndex >= 0 && noteIndex < MaxNumNotes)
		{
			noteData = m_CollectedNotes[noteIndex];
		}
		return noteData;
	}

	public void AddNote(Note note)
	{
		int noteIndex = note.Data.m_ID - 1;
		if (noteIndex >= 0 && noteIndex < MaxNumNotes)
		{
			m_CollectedNotes[noteIndex] = note.Data;
		}
		//Debug.Log(note.NoteContent);
		if(note.Data.m_Type==Note.NoteType.SpecialNote)
			SpecialNoteCount++;
		else if(note.Data.m_Type==Note.NoteType.ResearchNote)
			ResearchNoteCount++;
		else if(note.Data.m_Type==Note.NoteType.SpecialNote)
			RegularNoteCount++;

		Debug.Log("unlocked note " + noteIndex);
		m_InventoryMenu.UnlockNote(noteIndex);
	}

	public void AddBreakable()
	{
		BreakableObjectCount++;
		PersistentData.instance.SetThrowableCount(BreakableObjectCount);
	}

	public void AddKey()
	{
		KeyCount += 1;
	}

	public void UseKey()
	{
		KeyCount -= 1;
	}

	public void ResetKeys()
	{
		KeyCount = 0;
	}

	public void RemoveBreakable()
	{
		BreakableObjectCount--;
		PersistentData.instance.SetThrowableCount(BreakableObjectCount);
	}

	public void ShowInfo()
	{
		//Debug.Log(" Special : ",SpecialNoteCount," Research : ",ResearchNoteCount," Regular : ",RegularNoteCount," Breakable : ",BreakableObjectCount);
	}

	public void Reset()
	{
		ResetKeys();
		System.Array.ForEach(m_CollectedNotes, n => n = null);
		if (m_InventoryMenu != null)
		{
			Debug.Log("RESET INVENTORY");
			m_InventoryMenu.LockAllNotes();
		}

		BreakableObjectCount = 0;
		SpecialNoteCount = 0;
		ResearchNoteCount = 0;
		RegularNoteCount = 0;
		KeyCount = 0;
		CollectedMedicine = false;
		CollectedComputerFile = false;
		SavedKid = false;
		KidEscaped = false;
	}

	#endregion
}
