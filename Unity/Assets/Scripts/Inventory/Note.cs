﻿using UnityEngine;
using System.Collections;

public class Note : MonoBehaviour 
{
	#region Constants
	public enum NoteType{SpecialNote,ResearchNote,RegularNote};

	[System.Serializable()]
	public class NoteData
	{
		public NoteType m_Type = NoteType.RegularNote;
		public int m_ID = 0;
		public string m_Text = "Note text";
	}
	#endregion

	// Use this for initialization
	#region Variables
	[SerializeField] NoteData m_NoteData;
	#endregion

	#region Properties
	public NoteData Data { get { return m_NoteData; } }
	#endregion

	#region Functions

	#endregion
}
