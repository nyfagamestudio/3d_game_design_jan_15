﻿#pragma strict

var timer : float = 10.0;
var movementTime: float = 2;
var fpsController : FPSInputController;
var mouseLookController : MouseLook;
var mouseLookCamera : MouseLook;

function Start () 
{
	renderer.material.color.a = 1;
	fpsController.enabled = false;
	mouseLookController.enabled = false;
	mouseLookCamera.enabled = false;
	
	Fader();
	Mover();
}

function Fader()
{
	var currTime : float = 0;
	while (currTime < timer)
	{
		yield WaitForEndOfFrame();
		currTime = currTime + Time.deltaTime;
		renderer.material.color.a = Mathf.Lerp(1, 0, currTime / timer);
		//Debug.Log(currTime + " " + Time.deltaTime);
	}
}

function Mover()
{
	yield WaitForSeconds(movementTime);
	fpsController.enabled = true;
	mouseLookController.enabled = true;
	mouseLookCamera.enabled = true;
}

/*function Update ()
{
	timer = timer - Time.deltaTime;
	
	if(timer >= 0)
	
	{
	renderer.material.color.a -= 0.1 * Time.deltaTime;
	}
	
	if(timer <= 0)
	
	{
		GameObject.Find("First Person Controller").GetComponent(FPSInputController).enabled = true;
		GameObject.Find("First Person Controller").GetComponent(MouseLook).enabled = true;
		GameObject.Find("Main Camera").GetComponent(MouseLook).enabled = true;
		Destroy(gameObject);
	}
	
}*/