﻿using UnityEngine;
using System.Collections;

public class ToggleFlashLight : MonoBehaviour
{
	public Light flashlight;
	public bool on;

	// Use this for initialization
	void Start ()
	{
		flashlight.enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.F))
		{
			on = !on;
			GetComponent<AudioSource>().Play();
		}
			
		if (on)
		{
			flashlight.enabled = true;
		} 

		else if (!on) 
		{
			flashlight.enabled = false;
		}
			
	}
}
