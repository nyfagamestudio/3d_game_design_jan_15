﻿using UnityEngine;
using System.Collections;

public class Lean : MonoBehaviour 
{
	#region Variables
	public float m_LeanSpeed = 100;
	#endregion

	#region Functions
	// Use this for initialization
	void Start () 
	{
	   
	}
	
	// Update is called once per frame
	void Update () 
	{


		if (Input.GetKey(KeyCode.Q))
		{
			transform.Rotate(0, 0, m_LeanSpeed * Time.deltaTime);
			if((transform.eulerAngles.z>25)&&(transform.eulerAngles.z<90))
			{
				transform.eulerAngles = new Vector3(transform.eulerAngles.x,transform.eulerAngles.y,25);
			}
		}

		else
		{
			if (transform.eulerAngles.z<90)
			{
				transform.Rotate(0, 0, -m_LeanSpeed * Time.deltaTime);
			}
		}
		
		if (Input.GetKey(KeyCode.E))
		{
			transform.Rotate(0, 0, -m_LeanSpeed * Time.deltaTime);
			if((transform.eulerAngles.z<335)&&(transform.eulerAngles.z>90))
			{
				transform.eulerAngles = new Vector3(transform.eulerAngles.x,transform.eulerAngles.y,335);
			}
		}
		
		else
		{
			if (transform.eulerAngles.z>90)
			{
				transform.Rotate(0, 0, m_LeanSpeed * Time.deltaTime);
			}
		}

		if(Input.GetKey(KeyCode.Return))
			{
			CheckInventory();
			}
	}

	
	void CheckInventory()
	{
		FindObjectOfType<Inventory>().ShowInfo();
	}
	#endregion
}
