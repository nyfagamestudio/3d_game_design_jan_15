﻿using UnityEngine;
using System.Collections;

public class LockedDoor : MonoBehaviour 
{
	#region Variables
	[SerializeField] GameObject m_ObjectRemoved;
	[SerializeField] bool m_RequiresKey = true;
	[SerializeField] string OpenAnimName = "DoorOpen";
	#endregion

	#region Properties
	public bool RequiresKey
	{
		get { return m_RequiresKey; }
	}
	#endregion

	#region Functions
	public void Unlock()
	{
		if (m_ObjectRemoved != null)
		{
			m_ObjectRemoved.SetActive(false);
		}

		Animator DoorController = GetComponent (typeof(Animator)) as Animator;
		DoorController.Play (OpenAnimName);
	}
	#endregion
}
