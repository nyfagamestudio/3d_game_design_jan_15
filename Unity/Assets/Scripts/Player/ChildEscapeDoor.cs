﻿using UnityEngine;
using System.Collections;

public class ChildEscapeDoor : MonoBehaviour {

	public string m_EscapeTest = "The child is now safe";
	public float m_TextDuration = 3;

	// Use this for initialization
	void Start () {
	
	}

	public void ShowEscapedText()
	{
		StartCoroutine(ShowText());
	}

	IEnumerator ShowText()
	{
		PersistentData.instance.ShowInteractionText(true);
		PersistentData.instance.SetInteractionText(m_EscapeTest);
		yield return new WaitForSeconds(m_TextDuration);
		PersistentData.instance.ShowInteractionText(false);
	}
}
