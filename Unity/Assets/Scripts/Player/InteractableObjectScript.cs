﻿using UnityEngine;
using System.Collections;

public class InteractableObjectScript : MonoBehaviour {

	// Use this for initialization

	#region Variables
	public bool ItemDiscoverable = false;
	public bool ItemUsed = false;
	public enum InteractionType
	{
		// ALWAYS ADD TO THE END OF THIS LIST
		Note,
		Exit,
		Breakable,
		Key,
		LockedDoor,
		Medicine,
		ComputerFile,
		Hint,
		Child,
		ChildExit,
		// add new interaction types here
	};
	public InteractionType ObjectType;
	bool m_AllowInteraction = false;
	#endregion

	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (m_AllowInteraction && Input.GetKeyDown(KeyCode.LeftShift))
		{
			switch (ObjectType)
			{
			case InteractionType.Note:
				OnNotePickup();
				break;

			case InteractionType.Exit:
				OnLevelExitInteration();
				break;

			case InteractionType.Breakable:
				OnBreakablePickup();
				break;

			case InteractionType.Key:
				OnKeyPickup();
				break;

			case InteractionType.LockedDoor:
				LockedDoor door = GetComponent<LockedDoor>();
				if (PersistentData.instance.PlayerInventory.getKeyCount() > 0
				    || !door.RequiresKey)
				{
					OnUnlockDoor();
				}
				else
				{
					//audio.Play();
				}

				break;

			case InteractionType.Medicine:
				OnMedicinePickup();
				break;

			case InteractionType.ComputerFile:
				OnComputerFilePickup();
				break;

			case InteractionType.Child:
				OnSavedKid();
				break;

			case InteractionType.ChildExit:
				OnKidEscaped();
				break;

			case InteractionType.Hint:
				OnHintInteraction();
				break;
			}
		}
	}

	void OnTriggerEnter(Collider collider)
	{
		if (!enabled)
		{
			return;
		}

		if (collider.tag == "Player")
		{
			PersistentData.instance.ShowInteractionText(true);
			m_AllowInteraction = true;
			
			if(ObjectType == InteractionType.Note)
			{
				PersistentData.instance.SetInteractionText("Press Left Shift collect the note");
			}
			else if(ObjectType == InteractionType.Breakable)
			{
				PersistentData.instance.SetInteractionText("Press Left Shift collect the bottle");
			}
			else if(ObjectType == InteractionType.Exit)
			{
				PersistentData.instance.SetInteractionText("Press Left Shift to open");
			}
			else if(ObjectType == InteractionType.Key)
			{
				PersistentData.instance.SetInteractionText("Press Left Shift collect the key");
			}
			else if(ObjectType == InteractionType.LockedDoor)
			{
				LockedDoor door = GetComponent<LockedDoor>();
				if (PersistentData.instance.PlayerInventory.getKeyCount() > 0
				    || !door.RequiresKey)
				{
					PersistentData.instance.SetInteractionText("Press Left Shift to interact");
				}
				else
				{
					PersistentData.instance.SetInteractionText("Find a key to unlock this door");
				}
			}
			else if(ObjectType == InteractionType.Medicine)
			{
				PersistentData.instance.SetInteractionText("Press Left Shift collect the medicine");
			}
			else if(ObjectType == InteractionType.ComputerFile)
			{
				PersistentData.instance.SetInteractionText("Press Left Shift collect the file");
			}
			else if(ObjectType == InteractionType.Hint)
			{
				PersistentData.instance.SetInteractionText(GetComponent<Hint>().HintText);
			}
			else if(ObjectType == InteractionType.Child)
			{
				PersistentData.instance.SetInteractionText("Press Left Shift to save the child");
			}
			else if (ObjectType == InteractionType.ChildExit)
			{
				PersistentData.instance.SetInteractionText("Press Left Shift to help the child escape");
			}
		}
	}

	void OnTriggerExit(Collider collider)
	{
		if (collider.tag == "Player")
	    {
			PersistentData.instance.ShowInteractionText(false);
			m_AllowInteraction = false;
		}
	}

	void OnLevelExitInteration()
	{
		PersistentData.instance.ShowInteractionText(false);
		PersistentData.instance.SetInteractionText("");
		
		Application.LoadLevel(Application.loadedLevel + 1);
	}

	void OnNotePickup()
	{
		Note note = GetComponent<Note>();
		PersistentData.instance.PlayerInventory.AddNote(note);
		Destroy(gameObject);
		PersistentData.instance.ShowInteractionText(false);
	}

	void OnBreakablePickup()
	{
		PersistentData.instance.PlayerInventory.AddBreakable();
		Destroy (gameObject);
		PersistentData.instance.ShowInteractionText(false);
	}

	void OnKeyPickup()
	{
		PersistentData.instance.PlayerInventory.AddKey();
		Destroy (gameObject);
		PersistentData.instance.ShowInteractionText(false);
	}

	void OnBreakableThrow()
	{
		PersistentData.instance.PlayerInventory.RemoveBreakable();
	}

	void OnUnlockDoor()
	{
		GetComponent<LockedDoor>().Unlock();
		PersistentData.instance.PlayerInventory.UseKey();
		//gameObject.SetActive(false);
		PersistentData.instance.ShowInteractionText(false);
		enabled = false;
	}

	void OnMedicinePickup()
	{
		PersistentData.instance.PlayerInventory.CollectedMedicine = true;
		PersistentData.instance.ShowInteractionText(false);
		Destroy(gameObject);
	}

	void OnComputerFilePickup()
	{
		PersistentData.instance.PlayerInventory.CollectedComputerFile = true;
		PersistentData.instance.ShowInteractionText(false);
		Destroy(gameObject);
	}

	void OnSavedKid()
	{
		PersistentData.instance.PlayerInventory.SavedKid = true;
		PersistentData.instance.ShowInteractionText(false);
		Destroy(gameObject);
	}

	void OnKidEscaped()
	{
		PersistentData.instance.PlayerInventory.KidEscaped = true;
		PersistentData.instance.ShowInteractionText(false);

		enabled = false;

		ChildEscapeDoor door = GetComponent<ChildEscapeDoor>();
		door.ShowEscapedText();
		Destroy(gameObject, door.m_TextDuration);
	}

	void OnHintInteraction()
	{
		SendMessage("Interact", SendMessageOptions.DontRequireReceiver);
	}

}
