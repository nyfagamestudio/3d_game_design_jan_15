﻿using UnityEngine;
using System.Collections;

public class Throw : MonoBehaviour {

	// Use this for initialization
	#region variables
	public Rigidbody Throwable;
	public Camera ThrowCam;
	public Ray MousePositionRecorder;
	float m_ThrowForce = 20;
//	public Vector3 PointOfImpact;
//	private Vector3 InternalPointOfImpact;
	#endregion

	#region functions
//	public Vector3 FindPointOfImpact(Rigidbody);
//	public void CreateRadiusSphere(Vector3);
	#endregion

	void Start () 
	{
		ThrowCam = Camera.main;
	}
	
	// Update is called once per frame
	void Update () 
	{

		if(Input.GetKeyUp(KeyCode.Mouse0) && !GameController.instance.IsMenuOpen()
		   && PersistentData.instance.PlayerInventory.getBreakableObjectCount() > 0)
		{
			PersistentData.instance.PlayerInventory.RemoveBreakable();

			Rigidbody ThrowableClone;
			ThrowableClone = Instantiate(Throwable,(transform.position+transform.forward*2),transform.rotation) as Rigidbody;
			//ThrowableClone.velocity = transform.TransformDirection(Vector3.forward*50);

			MousePositionRecorder = ThrowCam.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 1));
			//MousePositionRecorder = ThrowCam.ScreenPointToRay(Input.mousePosition);
			//ThrowableClone.velocity = MousePositionRecorder.direction*50;
			ThrowableClone.AddForce(transform.forward * m_ThrowForce, ForceMode.VelocityChange);
		}
	}
		
}