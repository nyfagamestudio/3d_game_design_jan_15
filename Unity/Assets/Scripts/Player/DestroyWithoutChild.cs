﻿using UnityEngine;
using System.Collections;

public class DestroyWithoutChild : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		if(!PersistentData.instance.PlayerInventory.SavedKid)
		{
			//Destroy(this.gameObject);
			GetComponent<InteractableObjectScript>().enabled=false;
		}
	}
	
	// Update is called once per frame

}
