﻿using UnityEngine;
using System.Collections;

public class Hint : MonoBehaviour 
{
	#region Variables
	[SerializeField] string m_HintText = "";
	#endregion

	#region Properties
	public string HintText { get { return m_HintText; } }
	#endregion

	#region Functions

	#endregion
}
