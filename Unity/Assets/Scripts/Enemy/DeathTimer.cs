﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DeathTimer : MonoBehaviour
{
    #region Variables
	//[SerializeField]
	//float m_SecondsBeforeDeath = 5;

	[SerializeField]
	string m_DeathMessage = "HIDE!";

    Queue<Timer> m_Timers = new Queue<Timer>();
    bool m_IsRunning = false;
    #endregion

	#region Properties
	public bool ShowText
	{
		get { return GetComponent<GUIText>().enabled; }
		set { GetComponent<GUIText>().enabled = value; }
	}
	#endregion

    #region Functions
    public void StartCountDown(Timer timer)
    {
        m_Timers.Enqueue(timer);
    }

    void Update()
    {
        if (m_Timers.Count > 0 && !m_IsRunning)
        {
            StartCoroutine(CheckTimer(m_Timers.Dequeue()));
        }
    }

    IEnumerator CheckTimer(Timer timer)
    {
        m_IsRunning = true;
		ShowText = true;
        while (timer.IsRunning)
        {
			GetComponent<GUIText>().text = string.Format("{0}\n{1} seconds until death!", m_DeathMessage, Mathf.Max(0, (timer.DeathCounter - timer.TimePassed)).ToString("f0"));
			if (timer.TimePassed >= timer.DeathCounter)
			{
				ShowText = false;

				// player lost
				GameController.instance.OnPlayerDeath("You must get out of view whenever an enemy spots you");
				m_Timers.Clear();
				break;
            }

            yield return new WaitForEndOfFrame();
        }
		ShowText = false;
        m_IsRunning = false;
    }

	void OnLevelWasLoaded()
	{
		m_IsRunning = false;
		ShowText = false;
		StopCoroutine("CheckTimer");
	}
    #endregion
}
