﻿using UnityEngine;
using System.Collections;

public class AiVision : MonoBehaviour
{
    #region Variables
    [SerializeField]
    float m_FieldOfView = 20;

    [SerializeField]
    float m_ViewDistance = 10;

	[SerializeField]
	GameObject m_VisionSource;

    [SerializeField]
    GameObject m_Target;

    Vector3 m_LeftSide;
    Vector3 m_RightSide;

	bool m_TargetIsVisible;
    #endregion

	#region Properties
	public bool TargetIsVisible { get { return m_TargetIsVisible; } }
	//Vector3 VisionSource
	#endregion

    #region Functions
    void Start()
    {
		if (m_VisionSource == null)
		{
			m_VisionSource = gameObject;
		}
        //Debug.Log(m_LeftSide);
    }

    public void SetTarget(GameObject target)
    {
        m_Target = target;
    }

    void Update()
    {
		bool wasTargetVisible = m_TargetIsVisible;
		m_TargetIsVisible = IsTargetInRange(m_Target) && IsTargetInView(m_Target);
		/*if (m_TargetIsVisible)
        {
			Debug.Log("I SEE YOU!!");
        }*/

		if (!wasTargetVisible && m_TargetIsVisible)
		{
			// they weren't visible last frame, but now they are
			SendMessage("SetState", Enemy.EnemyState.Attacking);
		}
		else if (wasTargetVisible && !m_TargetIsVisible)
		{
			// they were visible last frame, but not they aren't
			SendMessage("SetState", Enemy.EnemyState.Walking);
		}
    }

    void OnDrawGizmos()
    {
		if (m_VisionSource != null)
		{
			Gizmos.color = Color.red;
			m_LeftSide = Quaternion.AngleAxis(-m_FieldOfView, Vector3.up) * m_VisionSource.transform.forward;
			m_LeftSide *= m_ViewDistance;
			
			m_RightSide = Quaternion.AngleAxis(m_FieldOfView, Vector3.up) * m_VisionSource.transform.forward;
			m_RightSide *= m_ViewDistance;

			Gizmos.DrawRay(m_VisionSource.transform.position, m_LeftSide);
			Gizmos.DrawRay(m_VisionSource.transform.position, m_RightSide);
			Gizmos.color = Color.white;
		}
    }

    bool IsTargetInRange(GameObject target)
    {
		Vector3 toTarget = target.transform.position - m_VisionSource.transform.position;
        float angleBetweenMeAndTarget = Vector3.Angle(transform.forward, toTarget);

        return angleBetweenMeAndTarget <= m_FieldOfView && toTarget.magnitude <= m_ViewDistance;
    }

    public bool IsTargetInView(GameObject target)
    {
		Vector3 toTarget = target.transform.position - m_VisionSource.transform.position;
        RaycastHit hitInfo;
		if (Physics.Raycast(m_VisionSource.transform.position, toTarget, out hitInfo, m_ViewDistance))
        {
            if (hitInfo.transform.gameObject == m_Target)
            {
                return true;
            }
        }

        return false;
    }
    #endregion
}
