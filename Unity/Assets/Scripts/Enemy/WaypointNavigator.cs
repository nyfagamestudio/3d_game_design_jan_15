﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WaypointNavigator : MonoBehaviour 
{
	#region Constants
	public enum NavigationType
	{
		Loop,
		Ping_Pong,
		Stop_At_End
	}
	#endregion

	#region Variables
	[SerializeField]
	NavigationType m_NavType = NavigationType.Loop;

	[SerializeField]
	List<Waypoint> m_Path = new List<Waypoint>();
	bool m_IsNavigating = false;
	float m_MovementSpeed = 1;
	float m_TurnSpeed = 5;
	int m_PrevIndex = 0;
	int m_NextIndex = 1;
	bool m_IsReverse = false;
	Transform m_Navigator;
	#endregion

	#region Properties
	public bool HasWaypoints { get { return m_Path.Count > 0 ; } }
	Waypoint PrevWP { get { return m_Path[m_PrevIndex]; } }
	Waypoint NextWP { get { return m_Path[m_NextIndex]; } }
	#endregion

	#region Functions
	// Use this for initialization
	void Start () 
	{
		for (int i = 0; i < m_Path.Count; i++)
		{
			m_Path[i].SetId(i);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{

	}



	public void StartNavigaton(Transform navigator, float movementSpeed, float turnSpeed)
	{
		m_MovementSpeed = Mathf.Abs(movementSpeed);
		m_TurnSpeed = turnSpeed;
		m_Navigator = navigator;

		m_PrevIndex = FindClosestWaypoint(navigator.transform.position);
		if (m_PrevIndex != -1)
		{
			m_IsNavigating = true;
			m_NextIndex = m_PrevIndex + 1;
			if (m_NextIndex >= m_Path.Count)
			{
				m_NextIndex = 0;
			}
			MoveTowardWP(PrevWP, NextWP, m_MovementSpeed);
		}
	}

	void MoveTowardWP(Waypoint prev, Waypoint next, float speed)
	{
		StartCoroutine(MoveTowardWPCoroutine(prev, next, speed));
	}
		
	IEnumerator MoveTowardWPCoroutine(Waypoint prev, Waypoint next, float speed)
	{
		float timePassed = 0;
		float timeToReach = GetDistanceBetween(prev, next) / speed;

		while (timePassed < timeToReach)
		{
			m_Navigator.position = Vector3.Lerp(PrevWP.transform.position, NextWP.transform.position, timePassed / timeToReach);

			yield return new WaitForEndOfFrame();

			if (m_IsNavigating)
			{
				TurnTowardsWaypoint(next);
				timePassed += Time.deltaTime;
			}
		}

		yield return new WaitForSeconds(next.WaitTime);

		//yield return StartCoroutine();

		OnReachedWaypoint(next);
	}



	void OnReachedWaypoint(Waypoint waypointReached)
	{
		//int wpId = waypointReached.Id;

		switch (m_NavType)
		{
		case NavigationType.Loop:
			HandleLoopingPath(waypointReached);
			break;

		case NavigationType.Ping_Pong:
			HandlePingPongPath(waypointReached);
			break;

		case NavigationType.Stop_At_End:
			HandleOncePath(waypointReached);
			break;
		}

	}

	void HandleLoopingPath(Waypoint waypointReached)
	{
		// check if we're at the end
		if (IsAtPathEnd(waypointReached.Id))
		{
			m_PrevIndex = m_NextIndex;
			m_NextIndex = 0;
		}
		else
		{
			m_PrevIndex = m_NextIndex;
			m_NextIndex = waypointReached.Id + 1;
		}

		MoveTowardWP(PrevWP, NextWP, m_MovementSpeed);
	}

	void HandlePingPongPath(Waypoint waypointReached)
	{
		// check if we're at the end
		if (IsAtPathEnd(waypointReached.Id))
		{
			int temp = m_NextIndex;
			m_NextIndex = m_PrevIndex;
			m_PrevIndex = temp;
			m_IsReverse = true;
		}
		else if (IsAtPathBeginning(waypointReached.Id))
		{
			int temp = m_NextIndex;
			m_NextIndex = m_PrevIndex;
			m_PrevIndex = temp;
			m_IsReverse = false;
		}
		else
		{
			m_PrevIndex = m_NextIndex;
			if (m_IsReverse)
			{
				m_NextIndex = waypointReached.Id - 1;
			}
			else
			{
				m_NextIndex = waypointReached.Id + 1;
			}
		}

		MoveTowardWP(PrevWP, NextWP, m_MovementSpeed);
	}

	void HandleOncePath(Waypoint waypointReached)
	{
		// check if we're at the end
		if (IsAtPathEnd(waypointReached.Id))
		{
			m_IsNavigating = false;
		}
		else
		{
			m_PrevIndex = m_NextIndex;
			m_NextIndex = waypointReached.Id + 1;
			MoveTowardWP(PrevWP, NextWP, m_MovementSpeed);
		}
	}

	public void PauseNavigation(bool pause)
	{
		m_IsNavigating = !pause;
	}

	int FindClosestWaypoint(Vector3 position)
	{
		if (m_Path.Count < 1)
		{
			Debug.LogError("No path available for " + name);
			return -1;
		}

		int wpIndex = 0;
		float shortest = Vector3.Distance(m_Path[0].transform.position, position);
		for (int i = 1; i < m_Path.Count; i++)
		{
			float dist = Vector3.Distance(m_Path[i].transform.position, position);
			if (shortest > dist)
			{
				wpIndex = i;
				shortest = dist;
			}
		}

		return wpIndex;
	}

	float GetDistanceBetween(Waypoint wp1, Waypoint wp2)
	{
		return Vector3.Distance(wp1.transform.position, wp2.transform.position); 
	}

	bool IsAtPathEnd(int wpId)
	{
		return wpId == m_Path.Count - 1;
	}

	bool IsAtPathBeginning(int wpId)
	{
		return wpId == 0;
	}

	void TurnTowardsWaypoint(Waypoint wp)
	{
		Vector3 navigatorPos = m_Navigator.transform.position;
		navigatorPos.y = 0;

		Vector3 wpPos = wp.transform.position;
		wpPos.y = 0;

		Utils.TurnToFace(m_Navigator.gameObject, navigatorPos, wpPos, m_TurnSpeed);
	}
	#endregion
}
