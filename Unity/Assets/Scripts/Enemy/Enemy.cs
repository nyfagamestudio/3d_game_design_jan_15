﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(WaypointNavigator))]
public class Enemy : MonoBehaviour 
{
	#region Constants
	public enum EnemyState
	{
		Idle,
		Walking,
		Distracted,
		Attacking,
	}

	const string IdleAnim = "Idle";
	const string WalkAnim = "Walk";
	#endregion

	#region Variables
	[SerializeField]
	float m_MovementSpeed = 1;

	[SerializeField]
	float m_TurnSpeed = 5;

	[SerializeField]
	float m_GazeTurnSpeed = 2.5f;

	[SerializeField]
	float m_DistractionLengthInSeconds = 5;

	[SerializeField]
	GameObject m_Head;

	//[SerializeField]
	GameObject m_Player;
	
	EnemyState m_CurrentState = EnemyState.Idle;
	Vector3 m_DistractionPosition;
	Vector3 m_IdleGazePosition;

	WaypointNavigator m_Navigator;
	AiHearing m_Hearing;
	AiVision m_Vision;
	Timer m_AttackTimer;
	Animator m_Animator;
	AudioSource m_FootSteps;

	DeathTimer m_DeathTimer;
	#endregion

	#region Properties

	#endregion

	#region Functions
	// Use this for initialization
	void Awake()
	{
		m_Player = GameObject.FindGameObjectWithTag("Player");
		m_Navigator = GetComponent<WaypointNavigator>();
		m_Animator = GetComponentInChildren<Animator>();

		m_Animator.speed = m_MovementSpeed;
		
		if (m_Navigator.HasWaypoints)
		{
			m_Navigator.StartNavigaton(transform, m_MovementSpeed, m_TurnSpeed);
		}

		if (m_Hearing == null)
		{
			m_Hearing = GetComponentInChildren<AiHearing>();
		}

		if (m_Vision == null)
		{
			m_Vision = GetComponent<AiVision>();
		}

		m_Vision.SetTarget(m_Player);
		m_AttackTimer = GetComponent<Timer>();
		m_DeathTimer = FindObjectOfType<DeathTimer>();

		if (m_Head == null)
		{
			m_Head = gameObject;
		}

		m_IdleGazePosition = m_Head.transform.position + m_Head.transform.forward * 2;

		if (m_Navigator.HasWaypoints)
		{
			SetState(EnemyState.Walking);
		}
		else
		{
			SetState(EnemyState.Idle);
		}

		m_FootSteps = GetComponentInChildren<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (m_CurrentState == EnemyState.Attacking)
		{
			Utils.TurnToFace(m_Head, m_Player, m_GazeTurnSpeed);
		}
		else if (m_CurrentState == EnemyState.Distracted)
		{
			Utils.TurnToFace(m_Head, m_Head.transform.position, m_DistractionPosition, m_GazeTurnSpeed * 3);
		}
		else if (m_CurrentState == EnemyState.Idle)
		{
			Utils.TurnToFace(m_Head, m_Head.transform.position, m_IdleGazePosition, m_GazeTurnSpeed * 3);
		}
	}

	public void SetState(EnemyState state)
	{
		if (m_CurrentState == state)
		{
			return;
		}

		m_CurrentState = state;

		Debug.Log(name + " enemy State "  + m_CurrentState);

		switch (state) 
		{
		case EnemyState.Idle:
			HandleStateChange(false, true, false, IdleAnim);
			if (m_FootSteps != null)
			{
				m_FootSteps.Stop();
			}
			break;
			
		case EnemyState.Walking:
			if (m_Navigator.HasWaypoints)
			{
				HandleStateChange(true, true, false, WalkAnim);
			}
			else
			{
				SetState(EnemyState.Idle);
			}
			break;
			
		case EnemyState.Distracted:
			HandleStateChange(false, false, false, IdleAnim);
			break;
			
		case EnemyState.Attacking:
			HandleStateChange(false, true, true, IdleAnim);
			break;
		}
	}
	
	void HandleStateChange(bool shouldPath, bool shouldSense, bool shouldStartAttackTimer, string animName)
	{
		m_Navigator.PauseNavigation(!shouldPath);
		m_Hearing.enabled = shouldSense;

		if (m_FootSteps != null)
		{
			if (animName == WalkAnim)
			{
				m_FootSteps.Play();
			}
			else
			{
				m_FootSteps.Stop();
			}
		}

		if (m_Animator != null)
		{
			m_Animator.Play(animName);
		}

		if (shouldStartAttackTimer)
		{
			StopCoroutine("DistractCorotine");
			m_AttackTimer.StartTimer();
			m_DeathTimer.StartCountDown(m_AttackTimer);
		}
		else
		{
			m_AttackTimer.StopTimer();
		}
		//m_Vision.enabled = shouldSense;
	}

	public void Distract(Vector3 distractionPosition)
	{
		if (m_CurrentState != EnemyState.Attacking)
		{
			SetState(EnemyState.Distracted);
			StartCoroutine(DistractCorotine(m_DistractionLengthInSeconds));
			m_DistractionPosition = distractionPosition;
			m_DistractionPosition.y = transform.position.y;
			//Debug.Log("m_DistractionPosition: " + m_DistractionPosition);
		}
	}
	
	IEnumerator DistractCorotine(float distractionSeconds)
	{
		yield return new WaitForSeconds(m_DistractionLengthInSeconds);
		if (m_CurrentState != EnemyState.Attacking)
		{
			SetState(EnemyState.Walking);
		}
	}
	#endregion
}
