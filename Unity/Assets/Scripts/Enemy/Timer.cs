﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour
{
    #region Variables
	[SerializeField] float m_DeathCounter = 5;
    float m_TimePassed = 0;
    bool m_IsRunning = false;
    #endregion

    #region Properties
    //public float TimeLeft { get { return m_AttackSpeed - m_TimePassed; } }
    public bool IsRunning { get { return m_IsRunning; } }
	public float TimePassed { get { return m_TimePassed; } }
	public float DeathCounter { get { return m_DeathCounter; } }
    #endregion

    #region Functions
    void Start()
    {
    }

    void Update()
    {
    }

    public void StartTimer()
    {
        if (!m_IsRunning)
        {
            m_IsRunning = true;
            m_TimePassed = 0;
            StartCoroutine(StartTimerCoroutine());
        }
    }

    IEnumerator StartTimerCoroutine()
    {
        m_TimePassed = 0;
		while (m_IsRunning)
        {
            m_TimePassed += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    public void StopTimer()
    {
        m_IsRunning = false;
        m_TimePassed = 0;
        StopCoroutine("StartTimerCoroutine");
    }
    #endregion
}
