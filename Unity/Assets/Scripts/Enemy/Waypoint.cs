﻿using UnityEngine;
using System.Collections;

public class Waypoint : MonoBehaviour 
{
	#region Constants

	#endregion

	#region Variables
	[SerializeField]
	float m_WaitTime = 3;

	[SerializeField]
	bool m_IsInvisible = true;

	int m_Id;
	#endregion

	#region Properties
	public float WaitTime { get { return m_WaitTime; } }
	public int Id { get { return m_Id; } }
	#endregion

	#region Functions
	void Start()
	{
		if (GetComponent<Renderer>() != null)
		{
			GetComponent<Renderer>().enabled = !m_IsInvisible;
		}
	}

	public void SetId(int id)
	{
		m_Id = id;
	}
	#endregion
}
