﻿using UnityEngine;
using System.Collections;

public class AiHearing : MonoBehaviour
{
    #region Variables
    [SerializeField]
    LayerMask m_ListenLayers;

    SphereCollider m_HearingRange;

	AiVision m_Vision;
    #endregion

    #region Function

    #endregion

    void Start()
    {
        m_HearingRange = GetComponent<Collider>() as SphereCollider;
        if (m_HearingRange == null)
        {
            Debug.LogError("no ai hearing for " + name);
            enabled = false;
        }

		m_Vision = GetComponentInParent<AiVision>();
		if (m_Vision == null)
		{
			Debug.LogError("m_Vision missing");
		}
    }

    void OnTriggerStay(Collider other)
    {
		//Debug.Log("other.gameObject.layer: " + other.gameObject.layer + " m_ListenLayers.value: " + m_ListenLayers.value);
		if (((1 << other.gameObject.layer) & m_ListenLayers.value) != 0)
		{
			if (m_Vision == null || m_Vision.IsTargetInView(other.transform.gameObject))
			{
				// instant loss
				GameController.instance.OnPlayerDeath("An enemy heard you because you were so close");
				GetComponent<Collider>().enabled = false;
			}
			else
			{
				// the player is in the hearing range of the enemy, but is behind an object
			}
		}
    }

	public void MakeNoise(Vector3 noisePosition)
	{
		// TODO: maybe check if it's in the hearing radius
		SendMessage("Distract", noisePosition);
	}
}
