//Maya ASCII 2014 scene
//Name: StandardRoom1.ma
//Last modified: Tue, Mar 03, 2015 03:36:21 PM
//Codeset: 1252
requires maya "2014";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014 x64";
fileInfo "cutIdentifier" "201303010241-864206";
fileInfo "osv" "Microsoft Windows 7 Home Premium Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
fileInfo "license" "education";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -5445.058718162968 1190.8260280508111 1947.2070480321629 ;
	setAttr ".r" -type "double3" -11.738352730751963 -65.799999999995634 1.9397267406862773e-015 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".fcp" 10000000;
	setAttr ".coi" 4993.5509461474958;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -996.21493530273426 110.00001507997513 -125.44589233397971 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "ground";
	setAttr ".t" -type "double3" 0 -0.5 0 ;
	setAttr ".s" -type "double3" 2000 20 2000 ;
	setAttr ".rp" -type "double3" 0 0.5 0 ;
	setAttr ".sp" -type "double3" 0 0.5 0 ;
createNode mesh -n "groundShape" -p "ground";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.011163413524627686 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 9 ".uvst[0].uvsp[0:8]" -type "float2" 0.90790725 0.99074042
		 0.086706311 0.9907403 0.091113217 0.98484254 0.90350252 0.98484063 0.90349996 0.011196613
		 0.49730694 0.49802032 0.091111392 0.011198521 0.90790695 0.0052988529 0.086706877
		 0.0052989125;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".vt[0:8]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5 0 0.5 0;
	setAttr -s 16 ".ed[0:15]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 2 8 0 8 5 0 3 8 0 4 8 0;
	setAttr -s 8 -ch 28 ".fc[0:7]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 3 15 13 -3
		mu 0 3 4 5 6
		f 4 2 9 -4 -9
		mu 0 4 4 6 8 7
		f 4 -12 -10 -8 -6
		mu 0 4 1 8 6 2
		f 4 10 4 6 8
		mu 0 4 7 0 3 4
		f 3 1 14 -13
		mu 0 3 3 2 5
		f 3 -15 7 -14
		mu 0 3 5 2 6
		f 3 12 -16 -7
		mu 0 3 3 5 4;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "ceiling";
	setAttr ".t" -type "double3" -10.000000000005684 249.49999999999997 -1.1823431123048067e-011 ;
	setAttr ".s" -type "double3" 2000 200 2000 ;
	setAttr ".rp" -type "double3" -999.9999999999992 0.50000000000005684 -999.99999999999989 ;
	setAttr ".sp" -type "double3" -0.5 0.5 -0.5 ;
	setAttr ".spt" -type "double3" -999.4999999999992 5.6843418860808015e-014 -999.49999999999989 ;
createNode mesh -n "ceilingShape" -p "ceiling";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode mesh -n "room2:polySurfaceShape19" -p "ceiling";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 5 ".uvst[0].uvsp[0:4]" -type "float2" 0.375 0.25 0.625
		 0.25 0.375 0.5 0.625 0.5 0.5 0.375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".vt[0:4]"  -0.5 0.5 0.5 0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 -0.5
		 0 0.5 0;
	setAttr -s 8 ".ed[0:7]"  0 1 0 2 3 0 0 2 0 1 3 0 0 4 1 4 3 1 1 4 1
		 2 4 1;
	setAttr -s 4 -ch 12 ".fc[0:3]" -type "polyFaces" 
		f 3 7 5 -2
		mu 0 3 2 4 3
		f 3 0 6 -5
		mu 0 3 0 1 4
		f 3 -7 3 -6
		mu 0 3 4 1 3
		f 3 4 -8 -3
		mu 0 3 0 4 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall";
createNode transform -n "wall4" -p "wall";
	setAttr ".t" -type "double3" 8 10.5 -9.5216263028929653 ;
	setAttr ".s" -type "double3" 250 250 10 ;
	setAttr ".rp" -type "double3" -8 -10.5 9.5216263028929689 ;
	setAttr ".sp" -type "double3" 0.5 -0.5 99.500000000000142 ;
	setAttr ".spt" -type "double3" -8.5 -10 -89.978373697107159 ;
createNode mesh -n "wallShape2" -p "wall4";
	setAttr -k off ".v";
	setAttr -s 8 ".iog";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50499718020393158 0.50718345129064168 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall9" -p "wall";
	setAttr ".t" -type "double3" -11.999999999999773 10.5 -26.921626302892264 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
	setAttr ".s" -type "double3" 250 250 10 ;
	setAttr ".rp" -type "double3" 999.50000000000045 239.49999254941935 -1000.578373697108 ;
	setAttr ".rpt" -type "double3" 12.499999999999426 0 2027.5 ;
	setAttr ".sp" -type "double3" 7.5000000000000577 0.49999997019767733 -0.4999999999994742 ;
	setAttr ".spt" -type "double3" 992.00000000000045 238.9999925792217 -1000.0783736971084 ;
createNode mesh -n "wallShape9" -p "wall9";
	setAttr -k off ".v";
	setAttr -s 4 ".iog";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.96874728798866272 0.51956410706043243 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.49999997 0.5
		 -0.5 0.5 -0.5 0.5 0.49999997 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
	setAttr ".dr" 1;
createNode transform -n "wall6" -p "wall";
	setAttr ".t" -type "double3" 8 10.5 -9.5216263028929653 ;
	setAttr ".s" -type "double3" 250 250 10 ;
	setAttr ".rp" -type "double3" -8 -10.5 9.5216263028929689 ;
	setAttr ".sp" -type "double3" -1.5 -0.5 99.500000000000142 ;
	setAttr ".spt" -type "double3" -6.5 -10 -89.978373697107159 ;
createNode transform -n "wall8" -p "wall";
	setAttr ".t" -type "double3" 8 10.5 -9.5216263028929653 ;
	setAttr ".s" -type "double3" 250 250 10 ;
	setAttr ".rp" -type "double3" 867 114.5 -985.47837369710885 ;
	setAttr ".spt" -type "double3" 867 114.5 -985.47837369710885 ;
createNode transform -n "wall2" -p "wall";
	setAttr ".t" -type "double3" 8 10.5 -9.5216263028929653 ;
	setAttr ".s" -type "double3" 250 250 10 ;
	setAttr ".rp" -type "double3" -633.00000000000011 114.5 -985.47837369710885 ;
	setAttr ".spt" -type "double3" -633.00000000000011 114.5 -985.47837369710885 ;
createNode transform -n "all11" -p "wall";
	setAttr ".t" -type "double3" -11.999999999999886 10.5 -26.921626302892037 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
	setAttr ".s" -type "double3" 250 250 10 ;
	setAttr ".rp" -type "double3" 999.50000000000284 239.49999254941935 -1000.578373697108 ;
	setAttr ".rpt" -type "double3" 12.499999999996923 0 2027.5 ;
	setAttr ".sp" -type "double3" 5.5000000000000462 0.49999997019767733 -0.49999999999950262 ;
	setAttr ".spt" -type "double3" 994.00000000000284 238.9999925792217 -1000.0783736971086 ;
createNode transform -n "wall13" -p "wall";
	setAttr ".t" -type "double3" -12 10.5 -26.921626302892037 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
	setAttr ".s" -type "double3" 250 250 10 ;
	setAttr ".rp" -type "double3" 999.50000000000352 239.49999254941935 -1000.5783736971084 ;
	setAttr ".rpt" -type "double3" 12.49999999999647 0 2027.5000000000005 ;
	setAttr ".sp" -type "double3" 3.5000000000000537 0.49999997019767733 -0.49999999999953104 ;
	setAttr ".spt" -type "double3" 996.00000000000352 238.9999925792217 -1000.0783736971088 ;
createNode transform -n "wall18" -p "wall";
	setAttr ".t" -type "double3" 8 10.5 -9.5216263028929653 ;
	setAttr ".s" -type "double3" -250 250 -10 ;
	setAttr ".rp" -type "double3" 742 239.5 1009.5216263028942 ;
	setAttr ".sp" -type "double3" -0.5 0.5 -0.49999999999998573 ;
	setAttr ".spt" -type "double3" 742.5 239 1010.0216263028942 ;
createNode transform -n "wall20" -p "wall";
	setAttr ".t" -type "double3" 8 10.5 -9.5216263028929653 ;
	setAttr ".s" -type "double3" -250 250 -10 ;
	setAttr ".rp" -type "double3" 117 114.5 1004.5216263028944 ;
	setAttr ".spt" -type "double3" 117 114.5 1004.5216263028944 ;
createNode transform -n "wall22" -p "wall";
	setAttr ".t" -type "double3" 8 10.5 -9.5216263028929653 ;
	setAttr ".s" -type "double3" -250 250 -10 ;
	setAttr ".rp" -type "double3" -383 114.5 1004.5216263028948 ;
	setAttr ".spt" -type "double3" -383 114.5 1004.5216263028948 ;
createNode transform -n "wall24" -p "wall";
	setAttr ".t" -type "double3" 8 10.5 -10.063247074922739 ;
	setAttr ".s" -type "double3" -250 250 -10 ;
	setAttr ".rp" -type "double3" -758.00000000000011 239.5 1009.521626302894 ;
	setAttr ".sp" -type "double3" -0.5 0.5 -0.49999999999998573 ;
	setAttr ".spt" -type "double3" -757.50000000000011 239 1010.021626302894 ;
createNode transform -n "wall25" -p "wall";
	setAttr ".t" -type "double3" 8.0000000000122782 10.499999999999917 -26.921626302881009 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
	setAttr ".s" -type "double3" -250 250 -10 ;
	setAttr ".rp" -type "double3" 999.49999999999102 239.50000000000009 1019.4216263028952 ;
	setAttr ".rpt" -type "double3" -2007.5000000000032 0 7.4999999999871454 ;
	setAttr ".sp" -type "double3" -0.50000000000000044 0.50000000000000011 -0.5 ;
	setAttr ".spt" -type "double3" 999.99999999999102 239.00000000000009 1019.9216263028952 ;
createNode transform -n "wall26" -p "wall";
	setAttr ".t" -type "double3" 8.0000000000104592 10.499992549419288 -276.92162630288226 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
	setAttr ".s" -type "double3" -250 250 -10 ;
	setAttr ".rp" -type "double3" 999.49999999999068 239.50000000000009 1019.4216263028944 ;
	setAttr ".rpt" -type "double3" -2007.5000000000016 0 7.4999999999888516 ;
	setAttr ".sp" -type "double3" -0.5 0.50000000000000011 -0.50000000000001421 ;
	setAttr ".spt" -type "double3" 999.99999999999068 239.00000000000009 1019.9216263028944 ;
createNode mesh -n "wallShape26" -p "wall26";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50499718020393158 0.50718345129064168 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall27" -p "wall";
	setAttr ".t" -type "double3" 8.0000000000050022 10.499999999999886 223.0783736971182 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
	setAttr ".s" -type "double3" -250 250 -10 ;
	setAttr ".rp" -type "double3" 249.49999999999051 239.49999254941943 1019.4216263028964 ;
	setAttr ".rpt" -type "double3" -1257.4999999999957 0 -742.50000000001342 ;
	setAttr ".sp" -type "double3" -1.4999999999999996 0.49999997019767778 -0.50000000000007105 ;
	setAttr ".spt" -type "double3" 250.99999999999051 238.99999257922175 1019.9216263028964 ;
createNode mesh -n "wallShape27" -p "wall27";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.96874728798866272 0.51956410706043243 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.49999997 0.5
		 -0.5 0.5 -0.5 0.5 0.49999997 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall7" -p "wall";
	setAttr ".t" -type "double3" -242 10.5 -9.5216263028931962 ;
	setAttr ".s" -type "double3" 250 250 10 ;
	setAttr ".rp" -type "double3" 992 239.5 -980.47837369710851 ;
	setAttr ".sp" -type "double3" 0.5 0.5 0.50000000000001421 ;
	setAttr ".spt" -type "double3" 991.5 239 -980.97837369710851 ;
createNode mesh -n "wallShape7" -p "wall7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50499718020393158 0.50718345129064168 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall1" -p "wall";
	setAttr ".t" -type "double3" -241.99999999999989 10.5 -9.5216263028930825 ;
	setAttr ".s" -type "double3" 250 250 10 ;
	setAttr ".rp" -type "double3" -508.00000000000023 239.5 -980.47837369710862 ;
	setAttr ".sp" -type "double3" 0.49999999999999911 0.5 0.50000000000001421 ;
	setAttr ".spt" -type "double3" -508.50000000000023 239 -980.97837369710862 ;
createNode mesh -n "wallShape1" -p "wall1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50499718020393158 0.50718345129064168 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall3" -p "wall";
	setAttr ".t" -type "double3" 258 10.499999999999972 -9.5216263028930825 ;
	setAttr ".s" -type "double3" 250 250 10 ;
	setAttr ".rp" -type "double3" -758.00000000000011 239.50000000000003 -980.47837369710862 ;
	setAttr ".sp" -type "double3" -0.5 0.50000000000000011 0.50000000000001421 ;
	setAttr ".spt" -type "double3" -757.50000000000011 239.00000000000003 -980.97837369710862 ;
createNode mesh -n "wallShape3" -p "wall3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50499718020393158 0.50718345129064168 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall5" -p "wall";
	setAttr ".t" -type "double3" 258 10.5 -9.5216263028930825 ;
	setAttr ".s" -type "double3" 250 250 10 ;
	setAttr ".rp" -type "double3" -258 239.5 -980.47837369710828 ;
	setAttr ".sp" -type "double3" -0.5 0.5 0.50000000000001421 ;
	setAttr ".spt" -type "double3" -257.5 239 -980.97837369710828 ;
createNode mesh -n "wallShape5" -p "wall5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50499718020393158 0.50718345129064168 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall10" -p "wall";
	setAttr ".t" -type "double3" -12.000000000000114 10.500007450580595 223.07837369711049 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
	setAttr ".s" -type "double3" 250 250 10 ;
	setAttr ".rp" -type "double3" 749.49999999999727 239.49998509883881 -1000.5783736971082 ;
	setAttr ".rpt" -type "double3" 262.50000000000279 0 1777.4999999999975 ;
	setAttr ".sp" -type "double3" 6.5000000000000338 0.499999940395355 -0.49999999999950262 ;
	setAttr ".spt" -type "double3" 742.99999999999727 238.99998515844345 -1000.0783736971088 ;
createNode mesh -n "wallShape10" -p "wall10";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.96874728798866272 0.51956410706043243 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.49999997 0.5
		 -0.5 0.5 -0.5 0.5 0.49999997 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall12" -p "wall";
	setAttr ".t" -type "double3" -11.999999999999886 10.500007450580595 223.07837369710455 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
	setAttr ".s" -type "double3" 250 250 10 ;
	setAttr ".rp" -type "double3" 749.50000000000148 239.49998509883881 -1000.5783736971082 ;
	setAttr ".rpt" -type "double3" 262.49999999999835 0 1777.5000000000036 ;
	setAttr ".sp" -type "double3" 4.5000000000000409 0.499999940395355 -0.49999999999950262 ;
	setAttr ".spt" -type "double3" 745.00000000000148 238.99998515844345 -1000.0783736971088 ;
createNode mesh -n "wallShape12" -p "wall12";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.96874728798866272 0.51956410706043243 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.49999997 0.5
		 -0.5 0.5 -0.5 0.5 0.49999997 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall14" -p "wall";
	setAttr ".t" -type "double3" -11.999999999999886 10.499992549419376 223.07837369710759 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
	setAttr ".s" -type "double3" 250 250 10 ;
	setAttr ".rp" -type "double3" 749.50000000000045 239.5 -1000.5783736971076 ;
	setAttr ".rpt" -type "double3" 262.49999999999937 0 1777.5 ;
	setAttr ".sp" -type "double3" 2.5000000000000444 0.49999999999999994 -0.4999999999994742 ;
	setAttr ".spt" -type "double3" 747.00000000000045 239 -1000.078373697108 ;
createNode mesh -n "wallShape14" -p "wall14";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.96874728798866272 0.51956410706043243 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.49999997 0.5
		 -0.5 0.5 -0.5 0.5 0.49999997 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall16" -p "wall";
	setAttr ".t" -type "double3" -12.000000000004547 10.499999999999943 723.07837369711842 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
	setAttr ".s" -type "double3" 250 250 10 ;
	setAttr ".rp" -type "double3" 249.49999999999113 239.49999254941943 -1000.578373697113 ;
	setAttr ".rpt" -type "double3" 762.5000000000133 0 1277.4999999999943 ;
	setAttr ".sp" -type "double3" 0.50000000000000044 0.49999997019767761 -0.5 ;
	setAttr ".spt" -type "double3" 248.99999999999113 238.99999257922175 -1000.078373697113 ;
createNode mesh -n "wallShape16" -p "wall16";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.96874728798866272 0.51956410706043243 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.49999997 0.5
		 -0.5 0.5 -0.5 0.5 0.49999997 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall17" -p "wall";
	setAttr ".t" -type "double3" 257.99999999999989 10.499992549419376 -9.5216263028943331 ;
	setAttr ".s" -type "double3" -250 250 -10 ;
	setAttr ".rp" -type "double3" 742 239.5 1009.5216263028942 ;
	setAttr ".sp" -type "double3" -0.5 0.5 -0.49999999999998573 ;
	setAttr ".spt" -type "double3" 742.5 239 1010.0216263028942 ;
createNode mesh -n "wallShape17" -p "wall17";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50499718020393158 0.50718345129064168 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall19" -p "wall";
	setAttr ".t" -type "double3" 258 10.5 -9.5216263028929689 ;
	setAttr ".s" -type "double3" -250 250 -10 ;
	setAttr ".rp" -type "double3" 242 239.5 1009.5216263028944 ;
	setAttr ".sp" -type "double3" -0.5 0.5 -0.49999999999998573 ;
	setAttr ".spt" -type "double3" 242.5 239 1010.0216263028944 ;
createNode mesh -n "wallShape19" -p "wall19";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50499718020393158 0.50718345129064168 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall21" -p "wall";
	setAttr ".t" -type "double3" 258 10.5 -9.5216263028930825 ;
	setAttr ".s" -type "double3" -250 250 -10 ;
	setAttr ".rp" -type "double3" -383 114.5 1004.5216263028948 ;
	setAttr ".spt" -type "double3" -383 114.5 1004.5216263028948 ;
createNode mesh -n "wallShape21" -p "wall21";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50499718020393158 0.50718345129064168 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall23" -p "wall";
	setAttr ".t" -type "double3" 258.00000000000011 10.5 -9.5216263028924004 ;
	setAttr ".s" -type "double3" -250 250 -10 ;
	setAttr ".rp" -type "double3" -758.00000000000011 239.5 1009.521626302894 ;
	setAttr ".sp" -type "double3" -0.5 0.5 -0.49999999999998573 ;
	setAttr ".spt" -type "double3" -757.50000000000011 239 1010.021626302894 ;
createNode mesh -n "wallShape23" -p "wall23";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50499718020393158 0.50718345129064168 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall29" -p "wall";
	setAttr ".t" -type "double3" 8.0000000000027285 10.499985098838664 223.07837369711919 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
	setAttr ".s" -type "double3" -250 250 -10 ;
	setAttr ".rp" -type "double3" -500.50000000000944 239.50000000000009 1019.4216263028958 ;
	setAttr ".rpt" -type "double3" -507.49999999999511 0 -1492.5000000000125 ;
	setAttr ".sp" -type "double3" -0.49999999999999822 0.50000000000000011 -0.50000000000001421 ;
	setAttr ".spt" -type "double3" -500.00000000000944 239.00000000000009 1019.9216263028958 ;
createNode mesh -n "wallShape29" -p "wall29";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.96874728798866272 0.51956410706043243 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.49999997 0.5
		 -0.5 0.5 -0.5 0.5 0.49999997 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall34" -p "wall";
	setAttr ".t" -type "double3" 8.0000000000050022 10.500007450580569 -276.92162630288601 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
	setAttr ".s" -type "double3" -250 250 -10 ;
	setAttr ".rp" -type "double3" -750.50000000000875 239.49999254941943 1019.4216263028956 ;
	setAttr ".rpt" -type "double3" -257.49999999999596 0 -1742.5000000000114 ;
	setAttr ".sp" -type "double3" 0.50000000000000044 0.49999997019767761 -0.49999999999998573 ;
	setAttr ".spt" -type "double3" -751.00000000000875 238.99999257922175 1019.9216263028956 ;
createNode mesh -n "wallShape34" -p "wall34";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.96874728798866272 0.51956410706043243 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.49999997 0.5
		 -0.5 0.5 -0.5 0.5 0.49999997 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall30" -p "wall";
	setAttr ".t" -type "double3" 3.4106051316484809e-013 1.4901161250691075e-005 -749.99999999999784 ;
	setAttr ".s" -type "double3" -1 1 1 ;
	setAttr ".rp" -type "double3" -1000.0000000000003 249.99999254941935 500.00000000000102 ;
	setAttr ".sp" -type "double3" 1000.0000000000003 249.99999254941935 500.00000000000102 ;
	setAttr ".spt" -type "double3" -2000.0000000000007 0 0 ;
createNode mesh -n "wallShape15" -p "wall30";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 140 ".uvst[0].uvsp[0:139]" -type "float2" 0.049214125 0.058149993
		 0.049213827 0.95621586 0.025142968 0.033362061 0.025141329 0.98100406 0.054576457
		 0.056351036 0.054576457 0.95442063 0.22714934 0.95673704 0.22986904 0.056351036 0.21246639
		 0.98135018 0.22986904 0.95442057 0.22714955 0.057629049 0.22714935 0.89863312 0.049213845
		 0.89817929 0.025141435 0.91976368 0.054576457 0.11438784 0.22986904 0.11438784 0.22714935
		 0.89863312 0.049213845 0.89817929 0.049214125 0.058149993 0.22714955 0.057629049
		 0.025141329 0.98100406 0.049213827 0.95621586 0.22714934 0.95673704 0.21246639 0.98135018
		 0.054576457 0.95442063 0.054576457 0.11438784 0.22986904 0.11438784 0.22986904 0.95442057
		 0.025142968 0.033362061 0.025141435 0.91976368 0.054576457 0.056351036 0.22986904
		 0.056351036 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.40189934 0.375 0.40189934
		 0.375 0.5 0.625 0.5 0.625 0.75 0.375 0.75 0.375 0.84810066 0.625 0.84810066 0.625
		 1 0.375 1 0.77689934 0 0.77689934 0.25 0.22310063 0 0.22310063 0.25 0.125 0 0.125
		 0.25 0.875 0 0.875 0.25 0.375 0 0.375 0.25 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5
		 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0.25 0.875 0 0.125 0 0.125 0.25 0.375
		 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1
		 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25
		 0.625 0.32594967 0.375 0.32594967 0.375 0.5 0.625 0.5 0.625 0.75 0.375 0.75 0.375
		 0.92405033 0.625 0.92405033 0.625 1 0.375 1 0.70094967 0 0.70094967 0.25 0.29905033
		 0 0.29905033 0.25 0.375 0.40189934 0.625 0.40189934 0.125 0 0.22310063 0 0.22310063
		 0.25 0.125 0.25 0.625 0.84810066 0.375 0.84810066 0.77689934 0.25 0.77689934 0 0.875
		 0 0.875 0.25 0.375 0 0.375 0.25 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75
		 0.625 0.75 0.375 1 0.625 1 0.875 0.25 0.875 0 0.125 0 0.125 0.25 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 84 ".vt[0:83]"  990 -2.8421709e-014 750 990 250 750 999.82995605 250 750
		 999.82995605 -2.8421709e-014 750 990 250 700 999.82995605 250 700 999.82995605 -2.8421709e-014 700
		 990 -2.8421709e-014 700 990 220 700 990 220 750 999.82995605 220 750 999.82995605 220 700
		 990 -2.8421709e-014 500 990 250 500 999.82995605 250 500 999.82995605 -2.8421709e-014 500
		 990 250 550 999.82995605 250 550 999.82995605 -2.8421709e-014 550 990 -2.8421709e-014 550
		 990 220 550 990 220 500 999.82995605 220 500 999.82995605 220 550 985.81958008 220 700
		 995.81958008 220 700 985.81958008 230 700 995.81958008 230 700 985.81958008 230 549.99987793
		 995.81958008 230 549.99987793 985.81958008 220 549.99987793 995.81958008 220 549.99987793
		 995.81958008 230 560.000061035156 985.81958008 230 560.000061035156 985.81958008 220 560.000061035156
		 995.81958008 220 560.000061035156 985.81958008 -5.0885208e-015 549.99987793 995.81958008 -9.218656e-015 549.99987793
		 985.81958008 230 549.99987793 995.81958008 230 549.99987793 985.81958008 230 559.99993896
		 995.81958008 230 559.99993896 985.81958008 -5.1810397e-015 559.99993896 995.81958008 -9.3017796e-015 559.99993896
		 985.81958008 6.4763008e-015 700 995.81958008 -4.6469384e-015 700 985.81958008 230 700
		 995.81958008 230 700 985.81958008 230 689.99993896 995.81958008 230 689.99993896
		 985.81958008 6.3837815e-015 689.99993896 995.81958008 -4.6469384e-015 689.99993896
		 995.81958008 220 700 1005.81964111 220 700 995.81958008 230 700 1005.81964111 230 700
		 995.81958008 230 549.99987793 1005.81964111 230 549.99987793 995.81958008 220 549.99987793
		 1005.81964111 220 549.99987793 1005.81964111 230 560.000061035156 995.81958008 230 560.000061035156
		 995.81958008 220 560.000061035156 1005.81964111 220 560.000061035156 1005.81964111 230 690.000061035156
		 995.81958008 230 690.000061035156 995.81958008 220 690.000061035156 1005.81964111 220 690.000061035156
		 995.81958008 -9.2186568e-015 550 1005.81964111 3.5157061e-016 550 995.81958008 230 550
		 1005.81964111 230 550 995.81958008 230 560.000061035156 1005.81964111 230 560.000061035156
		 995.81958008 -9.3017804e-015 560.000061035156 1005.81964111 4.4408979e-016 560.000061035156
		 995.81958008 0 700 1005.81964111 4.6931978e-015 700 995.81958008 230 700 1005.81964111 230 700
		 995.81958008 230 689.99993896 1005.81964111 230 689.99993896 995.81958008 0 689.99993896
		 1005.81964111 4.6931978e-015 689.99993896;
	setAttr -s 136 ".ed[0:135]"  1 4 0 0 9 0 1 2 0 2 10 0 3 0 0 2 5 0 3 6 0
		 0 7 0 4 5 0 5 11 0 7 8 0 8 4 0 20 8 0 9 1 0 8 9 0 10 3 0 9 10 0 11 6 0 10 11 0 13 16 0
		 12 21 0 13 14 0 14 22 0 15 12 0 14 17 0 15 18 0 12 19 0 16 4 0 17 5 0 16 17 0 17 23 0
		 19 20 0 20 16 0 21 13 0 20 21 0 22 15 0 21 22 0 23 18 0 22 23 0 23 11 0 24 25 0 26 27 0
		 28 29 0 30 31 0 24 26 0 25 27 0 26 33 0 27 32 0 28 30 0 29 31 0 30 34 0 31 35 0 32 29 0
		 33 28 0 32 33 0 34 24 0 33 34 0 35 25 0 34 35 0 35 32 0 36 37 0 38 39 0 40 41 0 42 43 0
		 36 38 0 37 39 0 38 40 0 39 41 0 40 42 0 41 43 0 42 36 0 43 37 0 44 45 0 46 47 0 48 49 0
		 50 51 0 44 46 0 45 47 0 46 48 0 47 49 0 48 50 0 49 51 0 50 44 0 51 45 0 52 53 0 54 55 0
		 56 57 0 58 59 0 52 54 0 53 55 0 54 65 0 55 64 0 56 58 0 57 59 0 58 62 0 59 63 0 60 57 0
		 61 56 0 60 61 0 62 66 0 61 62 0 63 67 0 62 63 0 63 60 0 64 60 0 65 61 0 64 65 0 66 52 0
		 65 66 0 67 53 0 66 67 0 67 64 0 68 69 0 70 71 0 72 73 0 74 75 0 68 70 0 69 71 0 70 72 0
		 71 73 0 72 74 0 73 75 0 74 68 0 75 69 0 76 77 0 78 79 0 80 81 0 82 83 0 76 78 0 77 79 0
		 78 80 0 79 81 0 80 82 0 81 83 0 82 76 0 83 77 0;
	setAttr -s 65 -ch 260 ".fc[0:64]" -type "polyFaces" 
		f 4 -11 -8 1 -15
		mu 0 4 11 10 0 12
		f 4 5 -9 -1 2
		mu 0 4 3 8 6 1
		f 4 6 -18 -19 15
		mu 0 4 5 9 15 14
		f 4 -16 -17 -2 -5
		mu 0 4 2 13 12 0
		f 4 -12 14 13 0
		mu 0 4 6 11 12 1
		f 4 -4 -3 -14 16
		mu 0 4 13 3 1 12
		f 4 -10 -6 3 18
		mu 0 4 15 7 4 14
		f 4 34 -21 26 31
		mu 0 4 16 17 18 19
		f 4 -22 19 29 -25
		mu 0 4 20 21 22 23
		f 4 -36 38 37 -26
		mu 0 4 24 25 26 27
		f 4 23 20 36 35
		mu 0 4 28 18 17 29
		f 4 -29 -30 27 8
		mu 0 4 8 23 22 6
		f 4 -28 -33 12 11
		mu 0 4 6 22 16 11
		f 4 -20 -34 -35 32
		mu 0 4 22 21 17 16
		f 4 -37 33 21 22
		mu 0 4 29 17 21 20
		f 4 -39 -23 24 30
		mu 0 4 26 25 30 31
		f 4 -40 -31 28 9
		mu 0 4 15 26 31 7
		f 4 40 45 -42 -45
		mu 0 4 32 33 34 35
		f 4 41 47 54 -47
		mu 0 4 35 34 36 37
		f 4 42 49 -44 -49
		mu 0 4 38 39 40 41
		f 4 58 57 -41 -56
		mu 0 4 42 43 44 45
		f 4 -58 59 -48 -46
		mu 0 4 33 46 47 34
		f 4 55 44 46 56
		mu 0 4 48 32 35 49
		f 4 -55 52 -43 -54
		mu 0 4 37 36 39 38
		f 4 50 -57 53 48
		mu 0 4 50 48 49 51
		f 4 43 51 -59 -51
		mu 0 4 41 40 43 42
		f 4 -60 -52 -50 -53
		mu 0 4 47 46 52 53
		f 4 64 61 -66 -61
		mu 0 4 54 55 56 57
		f 4 66 62 -68 -62
		mu 0 4 55 58 59 56
		f 4 68 63 -70 -63
		mu 0 4 58 60 61 59
		f 4 70 60 -72 -64
		mu 0 4 60 62 63 61
		f 4 65 67 69 71
		mu 0 4 57 56 64 65
		f 4 -69 -67 -65 -71
		mu 0 4 66 67 55 54
		f 4 72 77 -74 -77
		mu 0 4 68 69 70 71
		f 4 73 79 -75 -79
		mu 0 4 71 70 72 73
		f 4 74 81 -76 -81
		mu 0 4 73 72 74 75
		f 4 75 83 -73 -83
		mu 0 4 75 74 76 77
		f 4 -84 -82 -80 -78
		mu 0 4 69 78 79 70
		f 4 82 76 78 80
		mu 0 4 80 68 71 81
		f 4 84 89 -86 -89
		mu 0 4 82 83 84 85
		f 4 85 91 106 -91
		mu 0 4 85 84 86 87
		f 4 86 93 -88 -93
		mu 0 4 88 89 90 91
		f 4 110 109 -85 -108
		mu 0 4 92 93 94 95
		f 4 -110 111 -92 -90
		mu 0 4 83 96 97 84
		f 4 107 88 90 108
		mu 0 4 98 82 85 99
		f 4 -99 96 -87 -98
		mu 0 4 100 101 89 88
		f 4 94 -101 97 92
		mu 0 4 102 103 104 105
		f 4 87 95 -103 -95
		mu 0 4 91 90 106 107
		f 4 -104 -96 -94 -97
		mu 0 4 108 109 110 111
		f 4 -107 104 98 -106
		mu 0 4 87 86 101 100
		f 4 99 -109 105 100
		mu 0 4 103 98 99 104
		f 4 102 101 -111 -100
		mu 0 4 107 106 93 92
		f 4 -112 -102 103 -105
		mu 0 4 97 96 109 108
		f 4 116 113 -118 -113
		mu 0 4 112 113 114 115
		f 4 118 114 -120 -114
		mu 0 4 113 116 117 114
		f 4 120 115 -122 -115
		mu 0 4 116 118 119 117
		f 4 122 112 -124 -116
		mu 0 4 118 120 121 119
		f 4 117 119 121 123
		mu 0 4 115 114 122 123
		f 4 -121 -119 -117 -123
		mu 0 4 124 125 113 112
		f 4 124 129 -126 -129
		mu 0 4 126 127 128 129
		f 4 125 131 -127 -131
		mu 0 4 129 128 130 131
		f 4 126 133 -128 -133
		mu 0 4 131 130 132 133
		f 4 127 135 -125 -135
		mu 0 4 133 132 134 135
		f 4 -136 -134 -132 -130
		mu 0 4 127 136 137 128
		f 4 134 128 130 132
		mu 0 4 138 126 129 139;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall31" -p "wall";
	setAttr ".t" -type "double3" 8.0000000000105729 10.499992549419261 -526.92162630288237 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
	setAttr ".s" -type "double3" -250 250 -10 ;
	setAttr ".rp" -type "double3" 999.49999999999068 239.50000000000009 1019.4216263028944 ;
	setAttr ".rpt" -type "double3" -2007.5000000000016 0 7.4999999999888516 ;
	setAttr ".sp" -type "double3" -0.5 0.50000000000000011 -0.50000000000001421 ;
	setAttr ".spt" -type "double3" 999.99999999999068 239.00000000000009 1019.9216263028944 ;
createNode mesh -n "wallShape31" -p "wall31";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50499718020393158 0.50718345129064168 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall32" -p "wall";
	setAttr ".t" -type "double3" 8.0000000000028155 10.499985098838664 -26.921626302901075 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
	setAttr ".s" -type "double3" -250 250 -10 ;
	setAttr ".rp" -type "double3" -500.50000000000944 239.50000000000009 1019.4216263028958 ;
	setAttr ".rpt" -type "double3" -507.49999999999511 0 -1492.5000000000125 ;
	setAttr ".sp" -type "double3" -0.49999999999999822 0.50000000000000011 -0.50000000000001421 ;
	setAttr ".spt" -type "double3" -500.00000000000944 239.00000000000009 1019.9216263028958 ;
createNode mesh -n "wallShape32" -p "wall32";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.96874728798866272 0.51956410706043243 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.49999997 0.5
		 -0.5 0.5 -0.5 0.5 0.49999997 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "wall33" -p "wall";
	setAttr ".t" -type "double3" -12.00000000000446 10.499999999999943 473.07837369714639 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
	setAttr ".s" -type "double3" 250 250 10 ;
	setAttr ".rp" -type "double3" 249.49999999999113 239.49999254941943 -1000.578373697113 ;
	setAttr ".rpt" -type "double3" 762.5000000000133 0 1277.4999999999943 ;
	setAttr ".sp" -type "double3" 0.50000000000000044 0.49999997019767761 -0.5 ;
	setAttr ".spt" -type "double3" 248.99999999999113 238.99999257922175 -1000.078373697113 ;
createNode mesh -n "wallShape33" -p "wall33";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.96874728798866272 0.51956410706043243 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.049214125 0.058149993
		 0.96081978 0.055481076 0.9608199 0.95888615 0.049213827 0.95621586 0.98485309 0.031589717
		 0.025142968 0.033362061 0.98485237 0.98277718 0.025141329 0.98100406 0.054576457
		 0.056351036 0.9526422 0.056351036 0.9526422 0.95442063 0.054576457 0.95442063;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.49999997 0.5
		 -0.5 0.5 -0.5 0.5 0.49999997 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 8 9 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 1 4 6 2
		f 4 10 4 6 8
		mu 0 4 5 0 3 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".bw" 3;
createNode transform -n "door";
createNode transform -n "door1" -p "door";
	setAttr ".t" -type "double3" 1.1368683772161603e-013 0 500.00000000000466 ;
	setAttr ".rp" -type "double3" -996.21493530273437 110.00001507997513 -625.44589233398437 ;
	setAttr ".sp" -type "double3" -996.21493530273437 110.00001507997513 -625.44589233398437 ;
createNode mesh -n "doorShape1" -p "door1";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 7 ".pt";
	setAttr ".pt[396]" -type "float3" -25.300655 0 0 ;
	setAttr ".pt[397]" -type "float3" -25.300655 0 0 ;
	setAttr ".pt[398]" -type "float3" -25.300655 0 0 ;
	setAttr ".pt[399]" -type "float3" -25.300655 0 0 ;
createNode mesh -n "polySurfaceShape2" -p "door1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "f[0:421]" "f[426:451]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[422:425]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 632 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.41249466 0.7646122 0.41249466
		 0.98538589 0.58750153 0.98538589 0.58750153 0.7646122 0.58750153 0.0042739213 0.41249466
		 0.0042739213 0.41249466 0.24572611 0.58750153 0.24572611 0.36038589 0.24572611 0.36038589
		 0.0042739213 0.1396122 0.0042739213 0.1396122 0.24572611 0.8603878 0.0042739213 0.63961411
		 0.0042739213 0.63961411 0.24572611 0.8603878 0.24572611 0.58750153 0.26461411 0.41249466
		 0.26461411 0.41249466 0.4853878 0.58750153 0.4853878 0.58750153 0.50427389 0.41249466
		 0.50427389 0.41249466 0.74572611 0.58750153 0.74572605 0.375 0.76461196 0.375 0.98538232
		 0.58750153 2.9802322e-008 0.41249466 2.9802322e-008 0.375 0.0042738616 0.375 0.24572235
		 0.625 0.98538589 0.625 0.7646122 0.625 0.24572611 0.625 0.0042739213 0.375 0.26461387
		 0.375 0.48538423 0.41249472 0.25 0.58750153 0.25 0.625 0.4853878 0.625 0.26461411
		 0.375 0.50427383 0.375 0.74572235 0.41249549 0.5 0.58750165 0.5 0.625 0.74572605
		 0.625 0.50427389 0.41249466 0.75 0.58750153 0.74999994 0.375 0.3125 0.375 0.68843985
		 0.39583334 0.68843985 0.39583334 0.3125 0.41666669 0.68843985 0.41666669 0.3125 0.43750003
		 0.68843985 0.43750003 0.3125 0.45833337 0.68843985 0.45833337 0.3125 0.47916672 0.68843985
		 0.47916672 0.3125 0.50000006 0.68843985 0.50000006 0.3125 0.52083337 0.68843985 0.52083337
		 0.3125 0.54166669 0.68843985 0.54166669 0.3125 0.5625 0.68843985 0.5625 0.3125 0.58333331
		 0.68843985 0.58333331 0.3125 0.60416663 0.68843985 0.60416663 0.3125 0.62499994 0.68843985
		 0.62499994 0.3125 0.578125 0.020933539 0.5 0.15000001 0.63531649 0.078125 0.5 0 0.421875
		 0.020933539 0.36468354 0.078125 0.34375 0.15625 0.36468354 0.234375 0.421875 0.29156646
		 0.5 0.3125 0.578125 0.29156646 0.63531649 0.234375 0.65625 0.15625 0.63531649 0.921875
		 0.5 0.83749998 0.578125 0.97906649 0.5 1 0.421875 0.97906649 0.36468354 0.921875
		 0.34375 0.84375 0.36468354 0.765625 0.421875 0.70843351 0.5 0.6875 0.578125 0.70843351
		 0.63531649 0.765625 0.65625 0.84375 0.63531649 0.921875 0.63531649 0.921875 0.578125
		 0.97906649 0.578125 0.97906649 0.63531649 0.921875 0.578125 0.97906649 0.63531649
		 0.921875 0.578125 0.97906649 0.63531649 0.921875 0.578125 0.97906649 0.63531649 0.921875
		 0.578125 0.97906649 0.63531649 0.921875 0.578125 0.97906649 0.63531649 0.921875 0.578125
		 0.97906649 0.63531649 0.921875 0.578125 0.97906649 0.63531649 0.921875 0.578125 0.97906649
		 0.63531649 0.921875 0.578125 0.97906649 0.5 1 0.5 1 0.5 1 0.5 1 0.5 1 0.5 1 0.5 1
		 0.5 1 0.5 1 0.5 1 0.5 1 0.421875 0.97906649 0.421875 0.97906649 0.421875 0.97906649
		 0.421875 0.97906649 0.421875 0.97906649 0.421875 0.97906649 0.421875 0.97906649 0.421875
		 0.97906649 0.421875 0.97906649 0.421875 0.97906649 0.421875 0.97906649 0.36468354
		 0.921875 0.36468354 0.921875 0.36468354 0.921875 0.36468354 0.921875 0.36468354 0.921875
		 0.36468354 0.921875 0.36468354 0.921875 0.36468354 0.921875 0.36468354 0.921875 0.36468354
		 0.921875 0.36468354 0.921875 0.34375 0.84375 0.34375 0.84375 0.34375 0.84375 0.34375
		 0.84375 0.34375 0.84375 0.34375 0.84375 0.34375 0.84375 0.34375 0.84375 0.34375 0.84375
		 0.34375 0.84375 0.34375 0.84375 0.36468354 0.765625 0.36468354 0.765625 0.36468354
		 0.765625 0.36468354 0.765625 0.36468354 0.765625 0.36468354 0.765625 0.36468354 0.765625
		 0.36468354 0.765625 0.36468354 0.765625 0.36468354 0.765625 0.36468354 0.765625 0.421875
		 0.70843351 0.421875 0.70843351 0.421875 0.70843351 0.421875 0.70843351 0.421875 0.70843351
		 0.421875 0.70843351 0.421875 0.70843351 0.421875 0.70843351 0.421875 0.70843351 0.421875
		 0.70843351 0.421875 0.70843351 0.5 0.6875 0.5 0.6875 0.5 0.6875 0.5 0.6875 0.5 0.6875
		 0.5 0.6875 0.5 0.6875 0.5 0.6875 0.5 0.6875 0.5 0.6875 0.5 0.6875 0.578125 0.70843351
		 0.578125 0.70843351 0.578125 0.70843351 0.578125 0.70843351 0.578125 0.70843351 0.578125
		 0.70843351 0.578125 0.70843351 0.578125 0.70843351 0.578125 0.70843351 0.578125 0.70843351
		 0.578125 0.70843351 0.63531649 0.765625 0.63531649 0.765625 0.63531649 0.765625 0.63531649
		 0.765625 0.63531649 0.765625 0.63531649 0.765625 0.63531649 0.765625 0.63531649 0.765625
		 0.63531649 0.765625 0.63531649 0.765625 0.63531649 0.765625 0.65625 0.84375 0.65625
		 0.84375 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375
		 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375 0.41249466 0.7646122
		 0.41249466 0.98538589 0.58750153 0.98538589 0.58750153 0.7646122 0.58750153 0.0042739213
		 0.41249466 0.0042739213 0.41249466 0.24572611 0.58750153 0.24572611 0.36038589 0.24572611
		 0.36038589 0.0042739213 0.1396122 0.0042739213 0.1396122 0.24572611 0.8603878 0.0042739213
		 0.63961411 0.0042739213 0.63961411 0.24572611 0.8603878 0.24572611 0.58750153 0.26461411
		 0.41249466 0.26461411;
	setAttr ".uvst[0].uvsp[250:499]" 0.41249466 0.4853878 0.58750153 0.4853878
		 0.58750153 0.50427389 0.41249466 0.50427389 0.41249466 0.74572611 0.58750153 0.74572605
		 0.375 0.76461196 0.375 0.98538232 0.58750153 2.9802322e-008 0.41249466 2.9802322e-008
		 0.375 0.0042738616 0.375 0.24572235 0.625 0.98538589 0.625 0.7646122 0.625 0.24572611
		 0.625 0.0042739213 0.375 0.26461387 0.375 0.48538423 0.41249472 0.25 0.58750153 0.25
		 0.625 0.4853878 0.625 0.26461411 0.375 0.50427383 0.375 0.74572235 0.41249549 0.5
		 0.58750165 0.5 0.625 0.74572605 0.625 0.50427389 0.41249466 0.75 0.58750153 0.74999994
		 0.375 0.3125 0.39583334 0.3125 0.39583334 0.68843985 0.375 0.68843985 0.41666669
		 0.3125 0.41666669 0.68843985 0.43750003 0.3125 0.43750003 0.68843985 0.45833337 0.3125
		 0.45833337 0.68843985 0.47916672 0.3125 0.47916672 0.68843985 0.50000006 0.3125 0.50000006
		 0.68843985 0.52083337 0.3125 0.52083337 0.68843985 0.54166669 0.3125 0.54166669 0.68843985
		 0.5625 0.3125 0.5625 0.68843985 0.58333331 0.3125 0.58333331 0.68843985 0.60416663
		 0.3125 0.60416663 0.68843985 0.62499994 0.3125 0.62499994 0.68843985 0.578125 0.020933539
		 0.63531649 0.078125 0.5 0.15000001 0.5 0 0.421875 0.020933539 0.36468354 0.078125
		 0.34375 0.15625 0.36468354 0.234375 0.421875 0.29156646 0.5 0.3125 0.578125 0.29156646
		 0.63531649 0.234375 0.65625 0.15625 0.63531649 0.921875 0.578125 0.97906649 0.5 0.83749998
		 0.5 1 0.421875 0.97906649 0.36468354 0.921875 0.34375 0.84375 0.36468354 0.765625
		 0.421875 0.70843351 0.5 0.6875 0.578125 0.70843351 0.63531649 0.765625 0.65625 0.84375
		 0.63531649 0.921875 0.578125 0.97906649 0.578125 0.97906649 0.63531649 0.921875 0.578125
		 0.97906649 0.63531649 0.921875 0.578125 0.97906649 0.63531649 0.921875 0.578125 0.97906649
		 0.63531649 0.921875 0.578125 0.97906649 0.63531649 0.921875 0.578125 0.97906649 0.63531649
		 0.921875 0.578125 0.97906649 0.63531649 0.921875 0.578125 0.97906649 0.63531649 0.921875
		 0.578125 0.97906649 0.63531649 0.921875 0.578125 0.97906649 0.63531649 0.921875 0.5
		 1 0.5 1 0.5 1 0.5 1 0.5 1 0.5 1 0.5 1 0.5 1 0.5 1 0.5 1 0.5 1 0.421875 0.97906649
		 0.421875 0.97906649 0.421875 0.97906649 0.421875 0.97906649 0.421875 0.97906649 0.421875
		 0.97906649 0.421875 0.97906649 0.421875 0.97906649 0.421875 0.97906649 0.421875 0.97906649
		 0.421875 0.97906649 0.36468354 0.921875 0.36468354 0.921875 0.36468354 0.921875 0.36468354
		 0.921875 0.36468354 0.921875 0.36468354 0.921875 0.36468354 0.921875 0.36468354 0.921875
		 0.36468354 0.921875 0.36468354 0.921875 0.36468354 0.921875 0.34375 0.84375 0.34375
		 0.84375 0.34375 0.84375 0.34375 0.84375 0.34375 0.84375 0.34375 0.84375 0.34375 0.84375
		 0.34375 0.84375 0.34375 0.84375 0.34375 0.84375 0.34375 0.84375 0.36468354 0.765625
		 0.36468354 0.765625 0.36468354 0.765625 0.36468354 0.765625 0.36468354 0.765625 0.36468354
		 0.765625 0.36468354 0.765625 0.36468354 0.765625 0.36468354 0.765625 0.36468354 0.765625
		 0.36468354 0.765625 0.421875 0.70843351 0.421875 0.70843351 0.421875 0.70843351 0.421875
		 0.70843351 0.421875 0.70843351 0.421875 0.70843351 0.421875 0.70843351 0.421875 0.70843351
		 0.421875 0.70843351 0.421875 0.70843351 0.421875 0.70843351 0.5 0.6875 0.5 0.6875
		 0.5 0.6875 0.5 0.6875 0.5 0.6875 0.5 0.6875 0.5 0.6875 0.5 0.6875 0.5 0.6875 0.5
		 0.6875 0.5 0.6875 0.578125 0.70843351 0.578125 0.70843351 0.578125 0.70843351 0.578125
		 0.70843351 0.578125 0.70843351 0.578125 0.70843351 0.578125 0.70843351 0.578125 0.70843351
		 0.578125 0.70843351 0.578125 0.70843351 0.578125 0.70843351 0.63531649 0.765625 0.63531649
		 0.765625 0.63531649 0.765625 0.63531649 0.765625 0.63531649 0.765625 0.63531649 0.765625
		 0.63531649 0.765625 0.63531649 0.765625 0.63531649 0.765625 0.63531649 0.765625 0.63531649
		 0.765625 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375 0.65625
		 0.84375 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375
		 0.65625 0.84375 0.25 0.125 0.25 0.25 0.3125 0.25 0.3125 0.125 0.375 0.25 0.375 0.125
		 0.375 0.5625 0.625 0.5625 0.625 0.5 0.375 0.5 0.625 0.40189934 0.375 0.40189934 0.125
		 0.1875 0.125 0.25 0.22310063 0.25 0.22310063 0.1875 0.625 0.84810066 0.625 0.75 0.375
		 0.75 0.375 0.84810066 0.77689934 0.25 0.875 0.25 0.875 0.1875 0.77689934 0.1875 0.875
		 0.0625 0.77689934 0.0625 0.375 0.6875 0.625 0.6875 0.22310063 0.0625 0.125 0.0625
		 0.875 0 0.77689934 0 0.125 0 0.22310063 0 0.625 0.32594967 0.375 0.32594967;
	setAttr ".uvst[0].uvsp[500:631]" 0.375 0.40189934 0.625 0.40189934 0.29905033
		 0 0.22310063 0 0.22310063 0.25 0.29905033 0.25 0.625 0.92405033 0.625 0.84810066
		 0.375 0.84810066 0.375 0.92405033 0.77689934 0.25 0.77689934 0 0.70094967 0 0.70094967
		 0.25 0.375 0.5 0.625 0.5 0.625 0.5625 0.375 0.5625 0.625 0.40189934 0.375 0.40189934
		 0.125 0.1875 0.22310063 0.1875 0.22310063 0.25 0.125 0.25 0.375 0.75 0.625 0.75 0.625
		 0.84810066 0.375 0.84810066 0.77689934 0.25 0.77689934 0.1875 0.875 0.1875 0.875
		 0.25 0.875 0.0625 0.77689934 0.0625 0.625 0.6875 0.375 0.6875 0.22310063 0.0625 0.125
		 0.0625 0.77689934 0 0.875 0 0.125 0 0.22310063 0 0.22310063 0 0.22310063 0.25 0.29905033
		 0.25 0.29905033 0 0.375 0.84810066 0.375 0.92405033 0.625 0.92405033 0.625 0.84810066
		 0.70094967 0.25 0.77689934 0.25 0.77689934 0 0.70094967 0 0 0 0 0.13573955 1 0.13573955
		 1 0 0 0 1.9289279e-016 0.13573955 1 0.13573955 1 0 0 0 0 0.37637773 1 0.37637773
		 1 0 0.25 0.125 0.25 0.25 0.3125 0.25 0.3125 0.125 0.375 0.25 0.375 0.125 0.25 0.125
		 0.3125 0.125 0.3125 0.25 0.25 0.25 0.375 0.125 0.375 0.25 0.625 0 0.75 0 0.75 0.125
		 0.625 0.125 0.25 0 0.375 0 0.375 0.125 0.25 0.125 0.125 0 0.125 0.125 0.875 0 0.875
		 0.125 0.25 0.25 0.125 0.25 0.75 0.25 0.875 0.25 0.625 0.625 0.375 0.625 0.375 0.5
		 0.625 0.5 0.625 0.75 0.375 0.75 0.625 0.875 0.375 0.875 0.625 1 0.375 1 0.625 0.25
		 0.375 0.25 0.375 0.375 0.625 0.375 0.375 0.125 0.25 0.125 0.375 0.25 0.25 0.25 0.75
		 0.125 0.625 0.125 0.75 0.25 0.625 0.25 0 1 2.220446e-016 0 0.10219145 0 0.10219145
		 1 0 0 0.36363631 1.110223e-016 0.36363631 1 0 1 2.220446e-016 0 0.2043829 0 0.2043829
		 1 0 1 2.220446e-016 0 0.36363631 1.110223e-016 0.36363631 1 0 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 440 ".vt";
	setAttr ".vt[0:165]"  -995.49822998 71.95115662 -677.55615234 -996.13891602 71.3105011 -677.55615234
		 -996.13891602 71.95115662 -676.91552734 -999.12902832 71.3105011 -677.55615234 -999.76971436 71.95115662 -677.55615234
		 -999.12902832 71.95115662 -676.91552734 -996.13891602 108.7857132 -677.55615234 -995.49822998 108.14505005 -677.55615234
		 -996.13891602 108.14505005 -676.91552734 -999.76971436 108.14505005 -677.55615234
		 -999.12902832 108.7857132 -677.55615234 -999.12902832 108.14505005 -676.91552734
		 -996.13891602 108.14505005 -687.87664795 -995.49822998 108.14505005 -687.23602295
		 -996.13891602 108.7857132 -687.23602295 -999.76971436 108.14505005 -687.23602295
		 -999.12902832 108.14505005 -687.87664795 -999.12902832 108.7857132 -687.23602295
		 -996.13891602 71.3105011 -687.23602295 -995.49822998 71.95115662 -687.23602295 -996.13891602 71.95115662 -687.87664795
		 -999.76971436 71.95115662 -687.23602295 -999.12902832 71.3105011 -687.23602295 -999.12902832 71.95115662 -687.87664795
		 -996.34954834 86.81509399 -681.27648926 -996.34954834 87.47425079 -681.93566895 -996.34954834 88.37468719 -682.17700195
		 -996.34954834 89.27511597 -681.93566895 -996.34954834 89.9342804 -681.27648926 -996.34954834 90.17554474 -680.37609863
		 -996.34954834 89.9342804 -679.47570801 -996.34954834 89.27511597 -678.81652832 -996.34954834 88.37468719 -678.57525635
		 -996.34954834 87.47425079 -678.81652832 -996.34954834 86.81509399 -679.47570801 -996.34954834 86.57382965 -680.37609863
		 -999.95117188 86.81509399 -681.27648926 -999.95117188 87.47425079 -681.93566895 -999.95117188 88.37468719 -682.17700195
		 -999.95117188 89.27511597 -681.93566895 -999.95117188 89.9342804 -681.27648926 -999.95117188 90.17554474 -680.37609863
		 -999.95117188 89.9342804 -679.47570801 -999.95117188 89.27511597 -678.81652832 -999.95117188 88.37468719 -678.57525635
		 -999.95117188 87.47425079 -678.81652832 -999.95117188 86.81509399 -679.47570801 -999.95117188 86.57382965 -680.37609863
		 -996.34954834 88.37468719 -680.37609863 -1003.09564209 86.81507111 -681.27661133
		 -1005.64459229 86.81507111 -681.27661133 -1007.67852783 86.81507111 -681.27661133
		 -1009.27813721 86.81507111 -681.27410889 -1010.61468506 86.81507111 -681.10784912
		 -1011.93621826 86.81507111 -680.21374512 -1012.77502441 86.81507111 -678.14117432
		 -1013.071044922 86.81507111 -674.94781494 -1013.11340332 86.81507111 -670.51373291
		 -1013.05267334 86.81507111 -664.91516113 -1013.0067749023 86.81507111 -658.29205322
		 -1003.09564209 87.47424316 -681.93572998 -1005.64459229 87.47424316 -681.93572998
		 -1007.67852783 87.47424316 -681.93572998 -1009.27923584 87.47424316 -681.93328857
		 -1010.69958496 87.47424316 -681.76159668 -1012.34960938 87.47424316 -680.72741699
		 -1013.40075684 87.47424316 -678.34863281 -1013.72784424 87.47424316 -675.0036010742
		 -1013.77258301 87.47424316 -670.51971436 -1013.71154785 87.47424316 -664.90795898
		 -1013.66577148 87.47424316 -658.28741455 -1012.10632324 88.37467194 -658.29840088
		 -1003.09564209 88.37467194 -682.17706299 -1005.64459229 88.37467194 -682.17706299
		 -1007.67852783 88.37467194 -682.17706299 -1009.27954102 88.37467194 -682.17456055
		 -1010.73065186 88.37467194 -682.00085449219 -1012.50085449 88.37467194 -680.91516113
		 -1013.62982178 88.37467194 -678.42462158 -1013.96826172 88.37467194 -675.023925781
		 -1014.013793945 88.37467194 -670.52185059 -1013.95294189 88.37467194 -664.90533447
		 -1013.90716553 88.37467194 -658.2857666 -1003.09564209 89.27510071 -681.93572998
		 -1005.64459229 89.27510071 -681.93572998 -1007.67852783 89.27510071 -681.93572998
		 -1009.27923584 89.27510071 -681.93328857 -1010.69958496 89.27510071 -681.76159668
		 -1012.34960938 89.27510071 -680.72741699 -1013.40075684 89.27510071 -678.34863281
		 -1013.72784424 89.27510071 -675.0036010742 -1013.77258301 89.27510071 -670.51971436
		 -1013.71154785 89.27510071 -664.90795898 -1013.66577148 89.27510071 -658.28741455
		 -1003.09564209 89.93425751 -681.27661133 -1005.64459229 89.93425751 -681.27661133
		 -1007.67852783 89.93425751 -681.27661133 -1009.27813721 89.93425751 -681.27410889
		 -1010.61468506 89.93425751 -681.10784912 -1011.93621826 89.93425751 -680.21374512
		 -1012.77502441 89.93425751 -678.14117432 -1013.071044922 89.93425751 -674.94781494
		 -1013.11340332 89.93425751 -670.51373291 -1013.05267334 89.93425751 -664.91516113
		 -1013.0067749023 89.93425751 -658.29205322 -1003.09564209 90.17552185 -680.3762207
		 -1005.64459229 90.17552185 -680.3762207 -1007.67852783 90.17552185 -680.3762207 -1009.27667236 90.17552185 -680.37365723
		 -1010.49871826 90.17552185 -680.21502686 -1011.3717041 90.17552185 -679.5123291 -1011.92028809 90.17552185 -677.85784912
		 -1012.17382813 90.17552185 -674.87176514 -1012.21282959 90.17552185 -670.50579834
		 -1012.15222168 90.17552185 -664.9251709 -1012.10632324 90.17552185 -658.29840088
		 -1003.09564209 89.93425751 -679.47576904 -1005.64459229 89.93425751 -679.47576904
		 -1007.67852783 89.93425751 -679.47576904 -1009.27526855 89.93425751 -679.4732666
		 -1010.38275146 89.93425751 -679.32202148 -1010.80706787 89.93425751 -678.81103516
		 -1011.065795898 89.93425751 -677.57446289 -1011.2767334 89.93425751 -674.7956543
		 -1011.31268311 89.93425751 -670.4977417 -1011.25195313 89.93425751 -664.93499756
		 -1011.20605469 89.93425751 -658.30462646 -1003.095703125 89.27510071 -678.81658936
		 -1005.64459229 89.27510071 -678.81658936 -1007.67852783 89.27510071 -678.81658936
		 -1009.27429199 89.27510071 -678.81408691 -1010.29766846 89.27510071 -678.66833496
		 -1010.39355469 89.27510071 -678.29754639 -1010.44006348 89.27510071 -677.3671875
		 -1010.61975098 89.27510071 -674.73999023 -1010.65350342 89.27510071 -670.49200439
		 -1010.59277344 89.27510071 -664.94219971 -1010.546875 89.27510071 -658.30926514 -1003.095703125 88.37467194 -678.57525635
		 -1005.64459229 88.37467194 -678.57525635 -1007.67852783 88.37467194 -678.57525635
		 -1009.27380371 88.37467194 -678.57275391 -1010.26678467 88.37467194 -678.42901611
		 -1010.24249268 88.37467194 -678.10943604 -1010.21099854 88.37467194 -677.29095459
		 -1010.3795166 88.37467194 -674.71936035 -1010.41223145 88.37467194 -670.48962402
		 -1010.3515625 88.37467194 -664.94464111 -1010.30548096 88.37467194 -658.31079102
		 -1003.095703125 87.47424316 -678.81658936 -1005.64459229 87.47424316 -678.81658936
		 -1007.67852783 87.47424316 -678.81658936 -1009.27429199 87.47424316 -678.81408691
		 -1010.29766846 87.47424316 -678.66833496 -1010.39355469 87.47424316 -678.29754639
		 -1010.44006348 87.47424316 -677.3671875 -1010.61975098 87.47424316 -674.73999023
		 -1010.65350342 87.47424316 -670.49200439 -1010.59277344 87.47424316 -664.94219971
		 -1010.546875 87.47424316 -658.30926514 -1003.09564209 86.81507111 -679.47576904 -1005.64459229 86.81507111 -679.47576904
		 -1007.67852783 86.81507111 -679.47576904 -1009.27526855 86.81507111 -679.4732666
		 -1010.38275146 86.81507111 -679.32202148 -1010.80706787 86.81507111 -678.81103516;
	setAttr ".vt[166:331]" -1011.065795898 86.81507111 -677.57446289 -1011.2767334 86.81507111 -674.7956543
		 -1011.31268311 86.81507111 -670.4977417 -1011.25195313 86.81507111 -664.93499756
		 -1011.20605469 86.81507111 -658.30462646 -1003.09564209 86.57380676 -680.3762207
		 -1005.64459229 86.57380676 -680.3762207 -1007.67852783 86.57380676 -680.3762207 -1009.27667236 86.57380676 -680.37365723
		 -1010.49871826 86.57380676 -680.21502686 -1011.3717041 86.57380676 -679.5123291 -1011.92028809 86.57380676 -677.85784912
		 -1012.17382813 86.57380676 -674.87176514 -1012.21282959 86.57380676 -670.50579834
		 -1012.15222168 86.57380676 -664.9251709 -1012.10632324 86.57380676 -658.29840088
		 -992.023193359 71.95115662 -677.55615234 -992.66387939 71.3105011 -677.55615234 -992.66387939 71.95115662 -676.91552734
		 -995.65405273 71.3105011 -677.55615234 -996.29473877 71.95115662 -677.55615234 -995.65405273 71.95115662 -676.91552734
		 -992.66387939 108.7857132 -677.55615234 -992.023193359 108.14505005 -677.55615234
		 -992.66387939 108.14505005 -676.91552734 -996.29473877 108.14505005 -677.55615234
		 -995.65405273 108.7857132 -677.55615234 -995.65405273 108.14505005 -676.91552734
		 -992.66387939 108.14505005 -687.87664795 -992.023193359 108.14505005 -687.23602295
		 -992.66387939 108.7857132 -687.23602295 -996.29473877 108.14505005 -687.23602295
		 -995.65405273 108.14505005 -687.87664795 -995.65405273 108.7857132 -687.23602295
		 -992.66387939 71.3105011 -687.23602295 -992.023193359 71.95115662 -687.23602295 -992.66387939 71.95115662 -687.87664795
		 -996.29473877 71.95115662 -687.23602295 -995.65405273 71.3105011 -687.23602295 -995.65405273 71.95115662 -687.87664795
		 -996.080322266 86.81509399 -681.27648926 -996.080322266 87.47425079 -681.93566895
		 -996.080322266 88.37468719 -682.17700195 -996.080322266 89.27511597 -681.93566895
		 -996.080322266 89.9342804 -681.27648926 -996.080322266 90.17554474 -680.37609863
		 -996.080322266 89.9342804 -679.47570801 -996.080322266 89.27511597 -678.81652832
		 -996.080322266 88.37468719 -678.57525635 -996.080322266 87.47425079 -678.81652832
		 -996.080322266 86.81509399 -679.47570801 -996.080322266 86.57382965 -680.37609863
		 -992.47869873 86.81509399 -681.27648926 -992.47869873 87.47425079 -681.93566895 -992.47869873 88.37468719 -682.17700195
		 -992.47869873 89.27511597 -681.93566895 -992.47869873 89.9342804 -681.27648926 -992.47869873 90.17554474 -680.37609863
		 -992.47869873 89.9342804 -679.47570801 -992.47869873 89.27511597 -678.81652832 -992.47869873 88.37468719 -678.57525635
		 -992.47869873 87.47425079 -678.81652832 -992.47869873 86.81509399 -679.47570801 -992.47869873 86.57382965 -680.37609863
		 -996.080322266 88.37468719 -680.37609863 -989.33422852 86.81507111 -681.27661133
		 -986.78527832 86.81507111 -681.27661133 -984.75134277 86.81507111 -681.27661133 -983.1517334 86.81507111 -681.27410889
		 -981.81518555 86.81507111 -681.10784912 -980.49365234 86.81507111 -680.21374512 -979.65484619 86.81507111 -678.14117432
		 -979.35882568 86.81507111 -674.94781494 -979.31646729 86.81507111 -670.51373291 -979.37719727 86.81507111 -664.91516113
		 -979.4230957 86.81507111 -658.29205322 -989.33422852 87.47424316 -681.93572998 -986.78527832 87.47424316 -681.93572998
		 -984.75134277 87.47424316 -681.93572998 -983.15063477 87.47424316 -681.93328857 -981.73028564 87.47424316 -681.76159668
		 -980.08026123 87.47424316 -680.72741699 -979.02911377 87.47424316 -678.34863281 -978.70202637 87.47424316 -675.0036010742
		 -978.6572876 87.47424316 -670.51971436 -978.71832275 87.47424316 -664.90795898 -978.76409912 87.47424316 -658.28741455
		 -980.32354736 88.37467194 -658.29840088 -989.33422852 88.37467194 -682.17706299 -986.78527832 88.37467194 -682.17706299
		 -984.75134277 88.37467194 -682.17706299 -983.15032959 88.37467194 -682.17456055 -981.69921875 88.37467194 -682.00085449219
		 -979.92901611 88.37467194 -680.91516113 -978.80004883 88.37467194 -678.42462158 -978.46160889 88.37467194 -675.023925781
		 -978.41607666 88.37467194 -670.52185059 -978.47692871 88.37467194 -664.90533447 -978.52270508 88.37467194 -658.2857666
		 -989.33422852 89.27510071 -681.93572998 -986.78527832 89.27510071 -681.93572998 -984.75134277 89.27510071 -681.93572998
		 -983.15063477 89.27510071 -681.93328857 -981.73028564 89.27510071 -681.76159668 -980.08026123 89.27510071 -680.72741699
		 -979.02911377 89.27510071 -678.34863281 -978.70202637 89.27510071 -675.0036010742
		 -978.6572876 89.27510071 -670.51971436 -978.71832275 89.27510071 -664.90795898 -978.76409912 89.27510071 -658.28741455
		 -989.33422852 89.93425751 -681.27661133 -986.78527832 89.93425751 -681.27661133 -984.75134277 89.93425751 -681.27661133
		 -983.1517334 89.93425751 -681.27410889 -981.81518555 89.93425751 -681.10784912 -980.49365234 89.93425751 -680.21374512
		 -979.65484619 89.93425751 -678.14117432 -979.35882568 89.93425751 -674.94781494 -979.31646729 89.93425751 -670.51373291
		 -979.37719727 89.93425751 -664.91516113 -979.4230957 89.93425751 -658.29205322 -989.33422852 90.17552185 -680.3762207
		 -986.78527832 90.17552185 -680.3762207 -984.75134277 90.17552185 -680.3762207 -983.15319824 90.17552185 -680.37365723
		 -981.93115234 90.17552185 -680.21502686 -981.058166504 90.17552185 -679.5123291 -980.50958252 90.17552185 -677.85784912
		 -980.25604248 90.17552185 -674.87176514 -980.21704102 90.17552185 -670.50579834 -980.27764893 90.17552185 -664.9251709
		 -980.32354736 90.17552185 -658.29840088 -989.33422852 89.93425751 -679.47576904 -986.78527832 89.93425751 -679.47576904
		 -984.75134277 89.93425751 -679.47576904 -983.15460205 89.93425751 -679.4732666 -982.047119141 89.93425751 -679.32202148
		 -981.62280273 89.93425751 -678.81103516 -981.36407471 89.93425751 -677.57446289 -981.15313721 89.93425751 -674.7956543
		 -981.1171875 89.93425751 -670.4977417 -981.17791748 89.93425751 -664.93499756 -981.22381592 89.93425751 -658.30462646
		 -989.33416748 89.27510071 -678.81658936 -986.78527832 89.27510071 -678.81658936 -984.75134277 89.27510071 -678.81658936
		 -983.15557861 89.27510071 -678.81408691 -982.13220215 89.27510071 -678.66833496 -982.036315918 89.27510071 -678.29754639
		 -981.98980713 89.27510071 -677.3671875 -981.81011963 89.27510071 -674.73999023 -981.77636719 89.27510071 -670.49200439
		 -981.83709717 89.27510071 -664.94219971 -981.88299561 89.27510071 -658.30926514 -989.33416748 88.37467194 -678.57525635
		 -986.78527832 88.37467194 -678.57525635 -984.75134277 88.37467194 -678.57525635 -983.15606689 88.37467194 -678.57275391
		 -982.16308594 88.37467194 -678.42901611 -982.18737793 88.37467194 -678.10943604 -982.21887207 88.37467194 -677.29095459
		 -982.050354004 88.37467194 -674.71936035 -982.01763916 88.37467194 -670.48962402
		 -982.078308105 88.37467194 -664.94464111 -982.12438965 88.37467194 -658.31079102
		 -989.33416748 87.47424316 -678.81658936;
	setAttr ".vt[332:439]" -986.78527832 87.47424316 -678.81658936 -984.75134277 87.47424316 -678.81658936
		 -983.15557861 87.47424316 -678.81408691 -982.13220215 87.47424316 -678.66833496 -982.036315918 87.47424316 -678.29754639
		 -981.98980713 87.47424316 -677.3671875 -981.81011963 87.47424316 -674.73999023 -981.77636719 87.47424316 -670.49200439
		 -981.83709717 87.47424316 -664.94219971 -981.88299561 87.47424316 -658.30926514 -989.33422852 86.81507111 -679.47576904
		 -986.78527832 86.81507111 -679.47576904 -984.75134277 86.81507111 -679.47576904 -983.15460205 86.81507111 -679.4732666
		 -982.047119141 86.81507111 -679.32202148 -981.62280273 86.81507111 -678.81103516
		 -981.36407471 86.81507111 -677.57446289 -981.15313721 86.81507111 -674.7956543 -981.1171875 86.81507111 -670.4977417
		 -981.17791748 86.81507111 -664.93499756 -981.22381592 86.81507111 -658.30462646 -989.33422852 86.57380676 -680.3762207
		 -986.78527832 86.57380676 -680.3762207 -984.75134277 86.57380676 -680.3762207 -983.15319824 86.57380676 -680.37365723
		 -981.93115234 86.57380676 -680.21502686 -981.058166504 86.57380676 -679.5123291 -980.50958252 86.57380676 -677.85784912
		 -980.25604248 86.57380676 -674.87176514 -980.21704102 86.57380676 -670.50579834 -980.27764893 86.57380676 -664.9251709
		 -980.32354736 86.57380676 -658.29840088 -995.25769043 121.75343323 -671.23083496
		 -995.25769043 121.75343323 -644.66107178 -995.25769043 201.75341797 -644.66107178
		 -995.25769043 201.75341797 -671.23083496 -995.25769043 121.75343323 -657.94592285
		 -995.25769043 201.75341797 -657.94592285 -991.10784912 202.36911011 -676.23083496
		 -1001.10809326 202.36911011 -676.23083496 -991.10784912 118.69681549 -676.23083496
		 -1001.10809326 118.69681549 -676.23083496 -991.10791016 202.36911011 -671.23077393
		 -1001.1081543 202.36911011 -671.23077393 -991.10791016 118.69681549 -671.23077393
		 -1001.1081543 118.69681549 -671.23077393 -1001.1081543 197.36909485 -671.23077393
		 -1001.10809326 197.36909485 -676.23083496 -991.10784912 197.36909485 -676.23083496
		 -991.10791016 197.36909485 -671.23077393 -1001.1081543 123.69680023 -671.23077393
		 -1001.10809326 123.6967926 -676.23083496 -991.10784912 123.6967926 -676.23083496
		 -991.10791016 123.69680023 -671.23077393 -1001.1081543 202.36911011 -644.66107178
		 -991.10791016 202.36911011 -644.66107178 -991.10791016 197.36909485 -644.66107178
		 -1001.1081543 197.36909485 -644.66107178 -991.10784912 202.36911011 -639.66101074
		 -1001.10809326 202.36911011 -639.66101074 -991.10784912 118.69682312 -639.66101074
		 -1001.10809326 118.69681549 -639.66101074 -991.10791016 118.69681549 -644.66107178
		 -1001.1081543 118.69681549 -644.66107178 -1001.10809326 197.36909485 -639.66101074
		 -991.10784912 197.36909485 -639.66101074 -1001.1081543 123.69680023 -644.66107178
		 -1001.10809326 123.6967926 -639.66101074 -991.10784912 123.6967926 -639.66101074
		 -991.10791016 123.69680023 -644.66107178 -994.39807129 121.75343323 -672.44616699
		 -994.39807129 121.75343323 -643.44580078 -994.39807129 201.75341797 -643.44580078
		 -994.39807129 201.75341797 -672.44616699 -994.39807129 121.75343323 -657.94592285
		 -994.39807129 201.75341797 -657.94592285 -999.29449463 121.75343323 -672.44616699
		 -999.29449463 121.75343323 -643.44580078 -999.29449463 201.75341797 -643.44580078
		 -999.29449463 201.75341797 -672.44616699 -999.29449463 121.75343323 -657.94592285
		 -999.29449463 201.75341797 -657.94592285 -993.81958008 1.4901161e-005 -690.44592285
		 -997.81958008 1.4901161e-005 -690.44592285 -993.81958008 220.000015258789 -690.44592285
		 -997.81958008 220.000015258789 -690.44592285 -993.81958008 220.000015258789 -560.44586182
		 -997.81958008 220.000015258789 -560.44586182 -993.81958008 1.4901161e-005 -560.44586182
		 -997.81958008 1.4901161e-005 -560.44586182 -993.81958008 220.000015258789 -625.44586182
		 -993.81958008 1.4901161e-005 -625.44586182 -997.81958008 1.4901161e-005 -625.44586182
		 -997.81958008 220.000015258789 -625.44586182 -993.81958008 110.000015258789 -690.44592285
		 -993.81958008 110.000015258789 -625.44586182 -993.81958008 110.000015258789 -560.44586182
		 -997.81958008 110.000015258789 -560.44586182 -997.81958008 110.000015258789 -625.44586182
		 -997.81958008 110.000015258789 -690.44592285 -993.81958008 118.69681549 -671.23077393
		 -993.81958008 118.69681549 -644.66107178 -993.81958008 198.69677734 -671.23077393
		 -993.81958008 198.69677734 -644.66107178 -997.81958008 118.69681549 -644.66107178
		 -997.81958008 118.69681549 -671.23077393 -997.81958008 198.69677734 -644.66107178
		 -997.81958008 198.69677734 -671.23077393;
	setAttr -s 883 ".ed";
	setAttr ".ed[0:165]"  18 22 0 22 3 0 3 1 0 1 18 0 5 11 0 11 8 0 8 2 0 2 5 0
		 7 13 0 13 19 0 19 0 0 0 7 0 21 15 0 15 9 0 9 4 0 4 21 0 10 17 0 17 14 0 14 6 0 6 10 0
		 16 23 0 23 20 0 20 12 0 12 16 0 1 0 0 19 18 0 2 1 0 3 5 0 0 2 0 8 7 0 4 3 0 22 21 0
		 5 4 0 9 11 0 7 6 0 14 13 0 6 8 0 11 10 0 10 9 0 15 17 0 13 12 0 20 19 0 12 14 0 17 16 0
		 16 15 0 21 23 0 18 20 0 23 22 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 30 0 30 31 0
		 31 32 0 32 33 0 33 34 0 34 35 0 35 24 0 36 37 0 37 38 0 38 39 0 39 40 0 40 41 0 41 42 0
		 42 43 0 43 44 0 44 45 0 45 46 0 46 47 0 47 36 0 24 36 0 25 37 0 26 38 0 27 39 0 28 40 0
		 29 41 0 30 42 0 31 43 0 32 44 0 33 45 0 34 46 0 35 47 0 48 24 0 48 25 0 48 26 0 48 27 0
		 48 28 0 48 29 0 48 30 0 48 31 0 48 32 0 48 33 0 48 34 0 48 35 0 36 49 0 49 50 0 50 51 0
		 51 52 0 52 53 0 53 54 0 54 55 0 55 56 0 56 57 0 57 58 0 58 59 0 37 60 0 60 61 0 61 62 0
		 62 63 0 63 64 0 64 65 0 65 66 0 66 67 0 67 68 0 68 69 0 69 70 0 49 60 0 50 61 0 51 62 0
		 52 63 0 53 64 0 54 65 0 55 66 0 56 67 0 57 68 0 58 69 0 59 70 0 70 71 0 59 71 0 38 72 0
		 72 73 0 73 74 0 74 75 0 75 76 0 76 77 0 77 78 0 78 79 0 79 80 0 80 81 0 81 82 0 60 72 0
		 61 73 0 62 74 0 63 75 0 64 76 0 65 77 0 66 78 0 67 79 0 68 80 0 69 81 0 70 82 0 82 71 0
		 39 83 0 83 84 0 84 85 0 85 86 0 86 87 0 87 88 0 88 89 0 89 90 0 90 91 0 91 92 0 92 93 0
		 72 83 0;
	setAttr ".ed[166:331]" 73 84 0 74 85 0 75 86 0 76 87 0 77 88 0 78 89 0 79 90 0
		 80 91 0 81 92 0 82 93 0 93 71 0 40 94 0 94 95 0 95 96 0 96 97 0 97 98 0 98 99 0 99 100 0
		 100 101 0 101 102 0 102 103 0 103 104 0 83 94 0 84 95 0 85 96 0 86 97 0 87 98 0 88 99 0
		 89 100 0 90 101 0 91 102 0 92 103 0 93 104 0 104 71 0 41 105 0 105 106 0 106 107 0
		 107 108 0 108 109 0 109 110 0 110 111 0 111 112 0 112 113 0 113 114 0 114 115 0 94 105 0
		 95 106 0 96 107 0 97 108 0 98 109 0 99 110 0 100 111 0 101 112 0 102 113 0 103 114 0
		 104 115 0 115 71 0 42 116 0 116 117 0 117 118 0 118 119 0 119 120 0 120 121 0 121 122 0
		 122 123 0 123 124 0 124 125 0 125 126 0 105 116 0 106 117 0 107 118 0 108 119 0 109 120 0
		 110 121 0 111 122 0 112 123 0 113 124 0 114 125 0 115 126 0 126 71 0 43 127 0 127 128 0
		 128 129 0 129 130 0 130 131 0 131 132 0 132 133 0 133 134 0 134 135 0 135 136 0 136 137 0
		 116 127 0 117 128 0 118 129 0 119 130 0 120 131 0 121 132 0 122 133 0 123 134 0 124 135 0
		 125 136 0 126 137 0 137 71 0 44 138 0 138 139 0 139 140 0 140 141 0 141 142 0 142 143 0
		 143 144 0 144 145 0 145 146 0 146 147 0 147 148 0 127 138 0 128 139 0 129 140 0 130 141 0
		 131 142 0 132 143 0 133 144 0 134 145 0 135 146 0 136 147 0 137 148 0 148 71 0 45 149 0
		 149 150 0 150 151 0 151 152 0 152 153 0 153 154 0 154 155 0 155 156 0 156 157 0 157 158 0
		 158 159 0 138 149 0 139 150 0 140 151 0 141 152 0 142 153 0 143 154 0 144 155 0 145 156 0
		 146 157 0 147 158 0 148 159 0 159 71 0 46 160 0 160 161 0 161 162 0 162 163 0 163 164 0
		 164 165 0 165 166 0 166 167 0 167 168 0 168 169 0 169 170 0 149 160 0 150 161 0 151 162 0
		 152 163 0 153 164 0 154 165 0;
	setAttr ".ed[332:497]" 155 166 0 156 167 0 157 168 0 158 169 0 159 170 0 170 71 0
		 47 171 0 171 172 0 172 173 0 173 174 0 174 175 0 175 176 0 176 177 0 177 178 0 178 179 0
		 179 180 0 180 181 0 160 171 0 161 172 0 162 173 0 163 174 0 164 175 0 165 176 0 166 177 0
		 167 178 0 168 179 0 169 180 0 170 181 0 181 71 0 171 49 0 172 50 0 173 51 0 174 52 0
		 175 53 0 176 54 0 177 55 0 178 56 0 179 57 0 180 58 0 181 59 0 200 204 0 204 185 0
		 185 183 0 183 200 0 187 193 0 193 190 0 190 184 0 184 187 0 189 195 0 195 201 0 201 182 0
		 182 189 0 203 197 0 197 191 0 191 186 0 186 203 0 192 199 0 199 196 0 196 188 0 188 192 0
		 198 205 0 205 202 0 202 194 0 194 198 0 183 182 0 201 200 0 184 183 0 185 187 0 182 184 0
		 190 189 0 186 185 0 204 203 0 187 186 0 191 193 0 189 188 0 196 195 0 188 190 0 193 192 0
		 192 191 0 197 199 0 195 194 0 202 201 0 194 196 0 199 198 0 198 197 0 203 205 0 200 202 0
		 205 204 0 206 207 0 207 208 0 208 209 0 209 210 0 210 211 0 211 212 0 212 213 0 213 214 0
		 214 215 0 215 216 0 216 217 0 217 206 0 218 219 0 219 220 0 220 221 0 221 222 0 222 223 0
		 223 224 0 224 225 0 225 226 0 226 227 0 227 228 0 228 229 0 229 218 0 206 218 0 207 219 0
		 208 220 0 209 221 0 210 222 0 211 223 0 212 224 0 213 225 0 214 226 0 215 227 0 216 228 0
		 217 229 0 230 206 0 230 207 0 230 208 0 230 209 0 230 210 0 230 211 0 230 212 0 230 213 0
		 230 214 0 230 215 0 230 216 0 230 217 0 218 231 0 231 232 0 232 233 0 233 234 0 234 235 0
		 235 236 0 236 237 0 237 238 0 238 239 0 239 240 0 240 241 0 219 242 0 242 243 0 243 244 0
		 244 245 0 245 246 0 246 247 0 247 248 0 248 249 0 249 250 0 250 251 0 251 252 0 231 242 0
		 232 243 0 233 244 0 234 245 0 235 246 0 236 247 0 237 248 0 238 249 0;
	setAttr ".ed[498:663]" 239 250 0 240 251 0 241 252 0 252 253 0 241 253 0 220 254 0
		 254 255 0 255 256 0 256 257 0 257 258 0 258 259 0 259 260 0 260 261 0 261 262 0 262 263 0
		 263 264 0 242 254 0 243 255 0 244 256 0 245 257 0 246 258 0 247 259 0 248 260 0 249 261 0
		 250 262 0 251 263 0 252 264 0 264 253 0 221 265 0 265 266 0 266 267 0 267 268 0 268 269 0
		 269 270 0 270 271 0 271 272 0 272 273 0 273 274 0 274 275 0 254 265 0 255 266 0 256 267 0
		 257 268 0 258 269 0 259 270 0 260 271 0 261 272 0 262 273 0 263 274 0 264 275 0 275 253 0
		 222 276 0 276 277 0 277 278 0 278 279 0 279 280 0 280 281 0 281 282 0 282 283 0 283 284 0
		 284 285 0 285 286 0 265 276 0 266 277 0 267 278 0 268 279 0 269 280 0 270 281 0 271 282 0
		 272 283 0 273 284 0 274 285 0 275 286 0 286 253 0 223 287 0 287 288 0 288 289 0 289 290 0
		 290 291 0 291 292 0 292 293 0 293 294 0 294 295 0 295 296 0 296 297 0 276 287 0 277 288 0
		 278 289 0 279 290 0 280 291 0 281 292 0 282 293 0 283 294 0 284 295 0 285 296 0 286 297 0
		 297 253 0 224 298 0 298 299 0 299 300 0 300 301 0 301 302 0 302 303 0 303 304 0 304 305 0
		 305 306 0 306 307 0 307 308 0 287 298 0 288 299 0 289 300 0 290 301 0 291 302 0 292 303 0
		 293 304 0 294 305 0 295 306 0 296 307 0 297 308 0 308 253 0 225 309 0 309 310 0 310 311 0
		 311 312 0 312 313 0 313 314 0 314 315 0 315 316 0 316 317 0 317 318 0 318 319 0 298 309 0
		 299 310 0 300 311 0 301 312 0 302 313 0 303 314 0 304 315 0 305 316 0 306 317 0 307 318 0
		 308 319 0 319 253 0 226 320 0 320 321 0 321 322 0 322 323 0 323 324 0 324 325 0 325 326 0
		 326 327 0 327 328 0 328 329 0 329 330 0 309 320 0 310 321 0 311 322 0 312 323 0 313 324 0
		 314 325 0 315 326 0 316 327 0 317 328 0 318 329 0 319 330 0 330 253 0;
	setAttr ".ed[664:829]" 227 331 0 331 332 0 332 333 0 333 334 0 334 335 0 335 336 0
		 336 337 0 337 338 0 338 339 0 339 340 0 340 341 0 320 331 0 321 332 0 322 333 0 323 334 0
		 324 335 0 325 336 0 326 337 0 327 338 0 328 339 0 329 340 0 330 341 0 341 253 0 228 342 0
		 342 343 0 343 344 0 344 345 0 345 346 0 346 347 0 347 348 0 348 349 0 349 350 0 350 351 0
		 351 352 0 331 342 0 332 343 0 333 344 0 334 345 0 335 346 0 336 347 0 337 348 0 338 349 0
		 339 350 0 340 351 0 341 352 0 352 253 0 229 353 0 353 354 0 354 355 0 355 356 0 356 357 0
		 357 358 0 358 359 0 359 360 0 360 361 0 361 362 0 362 363 0 342 353 0 343 354 0 344 355 0
		 345 356 0 346 357 0 347 358 0 348 359 0 349 360 0 350 361 0 351 362 0 352 363 0 363 253 0
		 353 231 0 354 232 0 355 233 0 356 234 0 357 235 0 358 236 0 359 237 0 360 238 0 361 239 0
		 362 240 0 363 241 0 365 368 0 365 366 0 366 369 0 367 364 0 368 364 0 369 367 0 368 369 0
		 370 380 0 371 379 0 372 376 0 373 377 0 375 371 0 374 370 0 374 381 0 376 377 0 377 382 0
		 378 375 0 379 383 0 378 379 1 380 384 0 379 380 1 381 385 0 380 381 1 382 378 0 383 373 0
		 382 383 1 384 372 0 383 384 1 385 376 0 384 385 1 375 374 0 381 388 0 378 389 0 381 378 0
		 386 375 0 387 374 0 387 388 0 388 389 0 389 386 0 386 387 0 390 397 0 391 396 0 392 394 0
		 393 395 0 386 391 0 387 390 0 395 398 0 396 399 0 389 396 1 397 400 0 396 397 1 388 401 0
		 397 388 1 398 389 0 399 393 0 398 399 1 400 392 0 399 400 1 401 394 0 400 401 1 376 394 0
		 377 395 0 376 377 0 398 382 0 401 385 0 394 395 0 370 371 0 372 373 0 394 395 0 392 393 0
		 390 391 0 385 382 0 398 401 0 403 406 0 403 404 0 404 407 0 405 402 0 406 402 0 407 405 0
		 406 407 0 409 412 0 409 410 0 410 413 0 411 408 0 412 408 0 413 411 0;
	setAttr ".ed[830:882]" 412 413 0 414 415 0 416 417 0 420 421 0 414 426 0 415 431 0
		 416 422 0 417 425 0 418 428 0 419 429 0 420 423 0 421 424 0 422 418 0 423 414 0 422 427 0
		 424 415 0 423 424 0 425 419 0 424 430 0 425 422 0 426 416 0 427 423 0 426 427 0 428 420 0
		 427 428 0 429 421 0 428 429 0 430 425 0 429 430 0 431 417 0 430 431 0 418 419 0 431 426 0
		 426 432 0 427 433 0 432 433 0 416 434 0 432 434 0 422 435 0 434 435 0 435 433 0 430 436 0
		 431 437 0 436 437 0 425 438 0 436 438 0 417 439 0 437 439 0 439 438 0 435 438 0 439 434 0
		 437 432 0 436 433 0;
	setAttr -s 452 -ch 1744 ".fc[0:451]" -type "polyFaces" 
		f 4 -4 -3 -2 -1
		mu 0 4 0 1 2 3
		f 4 -8 -7 -6 -5
		mu 0 4 4 5 6 7
		f 4 -12 -11 -10 -9
		mu 0 4 8 9 10 11
		f 4 -16 -15 -14 -13
		mu 0 4 12 13 14 15
		f 4 -20 -19 -18 -17
		mu 0 4 16 17 18 19
		f 4 -24 -23 -22 -21
		mu 0 4 20 21 22 23
		f 4 3 -26 10 -25
		mu 0 4 1 0 24 25
		f 4 7 -28 2 -27
		mu 0 4 5 4 26 27
		f 4 11 -30 6 -29
		mu 0 4 28 29 6 5
		f 4 15 -32 1 -31
		mu 0 4 30 31 3 2
		f 4 4 -34 14 -33
		mu 0 4 4 7 32 33
		f 4 8 -36 18 -35
		mu 0 4 34 35 18 17
		f 4 19 -38 5 -37
		mu 0 4 36 37 7 6
		f 4 16 -40 13 -39
		mu 0 4 16 19 38 39
		f 4 9 -42 22 -41
		mu 0 4 40 41 22 21
		f 4 23 -44 17 -43
		mu 0 4 42 43 19 18
		f 4 20 -46 12 -45
		mu 0 4 20 23 44 45
		f 4 0 -48 21 -47
		mu 0 4 46 47 23 22
		f 3 28 26 24
		mu 0 3 28 5 27
		f 3 27 32 30
		mu 0 3 26 4 33
		f 3 36 29 34
		mu 0 3 36 6 29
		f 3 33 37 38
		mu 0 3 32 7 37
		f 3 42 35 40
		mu 0 3 42 18 35
		f 3 39 43 44
		mu 0 3 38 19 43
		f 3 46 41 25
		mu 0 3 46 22 41
		f 3 45 47 31
		mu 0 3 44 23 47
		f 4 72 60 -74 -49
		mu 0 4 48 49 50 51
		f 4 73 61 -75 -50
		mu 0 4 51 50 52 53
		f 4 74 62 -76 -51
		mu 0 4 53 52 54 55
		f 4 75 63 -77 -52
		mu 0 4 55 54 56 57
		f 4 76 64 -78 -53
		mu 0 4 57 56 58 59
		f 4 77 65 -79 -54
		mu 0 4 59 58 60 61
		f 4 78 66 -80 -55
		mu 0 4 61 60 62 63
		f 4 79 67 -81 -56
		mu 0 4 63 62 64 65
		f 4 80 68 -82 -57
		mu 0 4 65 64 66 67
		f 4 81 69 -83 -58
		mu 0 4 67 66 68 69
		f 4 82 70 -84 -59
		mu 0 4 69 68 70 71
		f 4 83 71 -73 -60
		mu 0 4 71 70 72 73
		f 3 -86 84 48
		mu 0 3 74 75 76
		f 3 -87 85 49
		mu 0 3 77 75 74
		f 3 -88 86 50
		mu 0 3 78 75 77
		f 3 -89 87 51
		mu 0 3 79 75 78
		f 3 -90 88 52
		mu 0 3 80 75 79
		f 3 -91 89 53
		mu 0 3 81 75 80
		f 3 -92 90 54
		mu 0 3 82 75 81
		f 3 -93 91 55
		mu 0 3 83 75 82
		f 3 -94 92 56
		mu 0 3 84 75 83
		f 3 -95 93 57
		mu 0 3 85 75 84
		f 3 -96 94 58
		mu 0 3 86 75 85
		f 3 -85 95 59
		mu 0 3 76 75 86
		f 3 130 -130 -129
		mu 0 3 87 88 89
		f 3 129 -154 -153
		mu 0 3 89 88 90
		f 3 153 -177 -176
		mu 0 3 90 88 91
		f 3 176 -200 -199
		mu 0 3 91 88 92
		f 3 199 -223 -222
		mu 0 3 92 88 93
		f 3 222 -246 -245
		mu 0 3 93 88 94
		f 3 245 -269 -268
		mu 0 3 94 88 95
		f 3 268 -292 -291
		mu 0 3 95 88 96
		f 3 291 -315 -314
		mu 0 3 96 88 97
		f 3 314 -338 -337
		mu 0 3 97 88 98
		f 3 337 -361 -360
		mu 0 3 98 88 99
		f 3 360 -131 -372
		mu 0 3 99 88 87
		f 4 96 118 -108 -61
		mu 0 4 100 101 102 103
		f 4 97 119 -109 -119
		mu 0 4 101 104 105 102
		f 4 98 120 -110 -120
		mu 0 4 104 106 107 105
		f 4 99 121 -111 -121
		mu 0 4 106 108 109 107
		f 4 100 122 -112 -122
		mu 0 4 108 110 111 109
		f 4 101 123 -113 -123
		mu 0 4 110 112 113 111
		f 4 102 124 -114 -124
		mu 0 4 112 114 115 113
		f 4 103 125 -115 -125
		mu 0 4 114 116 117 115
		f 4 104 126 -116 -126
		mu 0 4 116 118 119 117
		f 4 105 127 -117 -127
		mu 0 4 118 120 121 119
		f 4 106 128 -118 -128
		mu 0 4 120 87 89 121
		f 4 107 142 -132 -62
		mu 0 4 103 102 122 123
		f 4 108 143 -133 -143
		mu 0 4 102 105 124 122
		f 4 109 144 -134 -144
		mu 0 4 105 107 125 124
		f 4 110 145 -135 -145
		mu 0 4 107 109 126 125
		f 4 111 146 -136 -146
		mu 0 4 109 111 127 126
		f 4 112 147 -137 -147
		mu 0 4 111 113 128 127
		f 4 113 148 -138 -148
		mu 0 4 113 115 129 128
		f 4 114 149 -139 -149
		mu 0 4 115 117 130 129
		f 4 115 150 -140 -150
		mu 0 4 117 119 131 130
		f 4 116 151 -141 -151
		mu 0 4 119 121 132 131
		f 4 117 152 -142 -152
		mu 0 4 121 89 90 132
		f 4 131 165 -155 -63
		mu 0 4 123 122 133 134
		f 4 132 166 -156 -166
		mu 0 4 122 124 135 133
		f 4 133 167 -157 -167
		mu 0 4 124 125 136 135
		f 4 134 168 -158 -168
		mu 0 4 125 126 137 136
		f 4 135 169 -159 -169
		mu 0 4 126 127 138 137
		f 4 136 170 -160 -170
		mu 0 4 127 128 139 138
		f 4 137 171 -161 -171
		mu 0 4 128 129 140 139
		f 4 138 172 -162 -172
		mu 0 4 129 130 141 140
		f 4 139 173 -163 -173
		mu 0 4 130 131 142 141
		f 4 140 174 -164 -174
		mu 0 4 131 132 143 142
		f 4 141 175 -165 -175
		mu 0 4 132 90 91 143
		f 4 154 188 -178 -64
		mu 0 4 134 133 144 145
		f 4 155 189 -179 -189
		mu 0 4 133 135 146 144
		f 4 156 190 -180 -190
		mu 0 4 135 136 147 146
		f 4 157 191 -181 -191
		mu 0 4 136 137 148 147
		f 4 158 192 -182 -192
		mu 0 4 137 138 149 148
		f 4 159 193 -183 -193
		mu 0 4 138 139 150 149
		f 4 160 194 -184 -194
		mu 0 4 139 140 151 150
		f 4 161 195 -185 -195
		mu 0 4 140 141 152 151
		f 4 162 196 -186 -196
		mu 0 4 141 142 153 152
		f 4 163 197 -187 -197
		mu 0 4 142 143 154 153
		f 4 164 198 -188 -198
		mu 0 4 143 91 92 154
		f 4 177 211 -201 -65
		mu 0 4 145 144 155 156
		f 4 178 212 -202 -212
		mu 0 4 144 146 157 155
		f 4 179 213 -203 -213
		mu 0 4 146 147 158 157
		f 4 180 214 -204 -214
		mu 0 4 147 148 159 158
		f 4 181 215 -205 -215
		mu 0 4 148 149 160 159
		f 4 182 216 -206 -216
		mu 0 4 149 150 161 160
		f 4 183 217 -207 -217
		mu 0 4 150 151 162 161
		f 4 184 218 -208 -218
		mu 0 4 151 152 163 162
		f 4 185 219 -209 -219
		mu 0 4 152 153 164 163
		f 4 186 220 -210 -220
		mu 0 4 153 154 165 164
		f 4 187 221 -211 -221
		mu 0 4 154 92 93 165
		f 4 200 234 -224 -66
		mu 0 4 156 155 166 167
		f 4 201 235 -225 -235
		mu 0 4 155 157 168 166
		f 4 202 236 -226 -236
		mu 0 4 157 158 169 168
		f 4 203 237 -227 -237
		mu 0 4 158 159 170 169
		f 4 204 238 -228 -238
		mu 0 4 159 160 171 170
		f 4 205 239 -229 -239
		mu 0 4 160 161 172 171
		f 4 206 240 -230 -240
		mu 0 4 161 162 173 172
		f 4 207 241 -231 -241
		mu 0 4 162 163 174 173
		f 4 208 242 -232 -242
		mu 0 4 163 164 175 174
		f 4 209 243 -233 -243
		mu 0 4 164 165 176 175
		f 4 210 244 -234 -244
		mu 0 4 165 93 94 176
		f 4 223 257 -247 -67
		mu 0 4 167 166 177 178
		f 4 224 258 -248 -258
		mu 0 4 166 168 179 177
		f 4 225 259 -249 -259
		mu 0 4 168 169 180 179
		f 4 226 260 -250 -260
		mu 0 4 169 170 181 180
		f 4 227 261 -251 -261
		mu 0 4 170 171 182 181
		f 4 228 262 -252 -262
		mu 0 4 171 172 183 182
		f 4 229 263 -253 -263
		mu 0 4 172 173 184 183
		f 4 230 264 -254 -264
		mu 0 4 173 174 185 184
		f 4 231 265 -255 -265
		mu 0 4 174 175 186 185
		f 4 232 266 -256 -266
		mu 0 4 175 176 187 186
		f 4 233 267 -257 -267
		mu 0 4 176 94 95 187
		f 4 246 280 -270 -68
		mu 0 4 178 177 188 189
		f 4 247 281 -271 -281
		mu 0 4 177 179 190 188
		f 4 248 282 -272 -282
		mu 0 4 179 180 191 190
		f 4 249 283 -273 -283
		mu 0 4 180 181 192 191
		f 4 250 284 -274 -284
		mu 0 4 181 182 193 192
		f 4 251 285 -275 -285
		mu 0 4 182 183 194 193
		f 4 252 286 -276 -286
		mu 0 4 183 184 195 194
		f 4 253 287 -277 -287
		mu 0 4 184 185 196 195
		f 4 254 288 -278 -288
		mu 0 4 185 186 197 196
		f 4 255 289 -279 -289
		mu 0 4 186 187 198 197
		f 4 256 290 -280 -290
		mu 0 4 187 95 96 198
		f 4 269 303 -293 -69
		mu 0 4 189 188 199 200
		f 4 270 304 -294 -304
		mu 0 4 188 190 201 199
		f 4 271 305 -295 -305
		mu 0 4 190 191 202 201
		f 4 272 306 -296 -306
		mu 0 4 191 192 203 202
		f 4 273 307 -297 -307
		mu 0 4 192 193 204 203
		f 4 274 308 -298 -308
		mu 0 4 193 194 205 204
		f 4 275 309 -299 -309
		mu 0 4 194 195 206 205
		f 4 276 310 -300 -310
		mu 0 4 195 196 207 206
		f 4 277 311 -301 -311
		mu 0 4 196 197 208 207
		f 4 278 312 -302 -312
		mu 0 4 197 198 209 208
		f 4 279 313 -303 -313
		mu 0 4 198 96 97 209
		f 4 292 326 -316 -70
		mu 0 4 200 199 210 211
		f 4 293 327 -317 -327
		mu 0 4 199 201 212 210
		f 4 294 328 -318 -328
		mu 0 4 201 202 213 212
		f 4 295 329 -319 -329
		mu 0 4 202 203 214 213
		f 4 296 330 -320 -330
		mu 0 4 203 204 215 214
		f 4 297 331 -321 -331
		mu 0 4 204 205 216 215
		f 4 298 332 -322 -332
		mu 0 4 205 206 217 216
		f 4 299 333 -323 -333
		mu 0 4 206 207 218 217
		f 4 300 334 -324 -334
		mu 0 4 207 208 219 218
		f 4 301 335 -325 -335
		mu 0 4 208 209 220 219
		f 4 302 336 -326 -336
		mu 0 4 209 97 98 220
		f 4 315 349 -339 -71
		mu 0 4 211 210 221 222
		f 4 316 350 -340 -350
		mu 0 4 210 212 223 221
		f 4 317 351 -341 -351
		mu 0 4 212 213 224 223
		f 4 318 352 -342 -352
		mu 0 4 213 214 225 224
		f 4 319 353 -343 -353
		mu 0 4 214 215 226 225
		f 4 320 354 -344 -354
		mu 0 4 215 216 227 226
		f 4 321 355 -345 -355
		mu 0 4 216 217 228 227
		f 4 322 356 -346 -356
		mu 0 4 217 218 229 228
		f 4 323 357 -347 -357
		mu 0 4 218 219 230 229
		f 4 324 358 -348 -358
		mu 0 4 219 220 231 230
		f 4 325 359 -349 -359
		mu 0 4 220 98 99 231
		f 4 338 361 -97 -72
		mu 0 4 222 221 101 100
		f 4 339 362 -98 -362
		mu 0 4 221 223 104 101
		f 4 340 363 -99 -363
		mu 0 4 223 224 106 104
		f 4 341 364 -100 -364
		mu 0 4 224 225 108 106
		f 4 342 365 -101 -365
		mu 0 4 225 226 110 108
		f 4 343 366 -102 -366
		mu 0 4 226 227 112 110
		f 4 344 367 -103 -367
		mu 0 4 227 228 114 112
		f 4 345 368 -104 -368
		mu 0 4 228 229 116 114
		f 4 346 369 -105 -369
		mu 0 4 229 230 118 116
		f 4 347 370 -106 -370
		mu 0 4 230 231 120 118
		f 4 348 371 -107 -371
		mu 0 4 231 99 87 120
		f 4 -376 -375 -374 -373
		mu 0 4 232 233 234 235
		f 4 -380 -379 -378 -377
		mu 0 4 236 237 238 239
		f 4 -384 -383 -382 -381
		mu 0 4 240 241 242 243
		f 4 -388 -387 -386 -385
		mu 0 4 244 245 246 247
		f 4 -392 -391 -390 -389
		mu 0 4 248 249 250 251
		f 4 -396 -395 -394 -393
		mu 0 4 252 253 254 255
		f 4 375 -398 382 -397
		mu 0 4 233 232 256 257
		f 4 379 -400 374 -399
		mu 0 4 237 236 258 259
		f 4 383 -402 378 -401
		mu 0 4 260 261 238 237
		f 4 387 -404 373 -403
		mu 0 4 262 263 235 234
		f 4 376 -406 386 -405
		mu 0 4 236 239 264 265
		f 4 380 -408 390 -407
		mu 0 4 266 267 250 249
		f 4 391 -410 377 -409
		mu 0 4 268 269 239 238
		f 4 388 -412 385 -411
		mu 0 4 248 251 270 271
		f 4 381 -414 394 -413
		mu 0 4 272 273 254 253
		f 4 395 -416 389 -415
		mu 0 4 274 275 251 250
		f 4 392 -418 384 -417
		mu 0 4 252 255 276 277
		f 4 372 -420 393 -419
		mu 0 4 278 279 255 254
		f 3 400 398 396
		mu 0 3 260 237 259
		f 3 399 404 402
		mu 0 3 258 236 265
		f 3 408 401 406
		mu 0 3 268 238 261
		f 3 405 409 410
		mu 0 3 264 239 269
		f 3 414 407 412
		mu 0 3 274 250 267
		f 3 411 415 416
		mu 0 3 270 251 275
		f 3 418 413 397
		mu 0 3 278 254 273
		f 3 417 419 403
		mu 0 3 276 255 279
		f 4 420 445 -433 -445
		mu 0 4 280 281 282 283
		f 4 421 446 -434 -446
		mu 0 4 281 284 285 282
		f 4 422 447 -435 -447
		mu 0 4 284 286 287 285
		f 4 423 448 -436 -448
		mu 0 4 286 288 289 287
		f 4 424 449 -437 -449
		mu 0 4 288 290 291 289
		f 4 425 450 -438 -450
		mu 0 4 290 292 293 291
		f 4 426 451 -439 -451
		mu 0 4 292 294 295 293
		f 4 427 452 -440 -452
		mu 0 4 294 296 297 295
		f 4 428 453 -441 -453
		mu 0 4 296 298 299 297
		f 4 429 454 -442 -454
		mu 0 4 298 300 301 299
		f 4 430 455 -443 -455
		mu 0 4 300 302 303 301
		f 4 431 444 -444 -456
		mu 0 4 302 304 305 303
		f 3 -421 -457 457
		mu 0 3 306 307 308
		f 3 -422 -458 458
		mu 0 3 309 306 308
		f 3 -423 -459 459
		mu 0 3 310 309 308
		f 3 -424 -460 460
		mu 0 3 311 310 308
		f 3 -425 -461 461
		mu 0 3 312 311 308
		f 3 -426 -462 462
		mu 0 3 313 312 308
		f 3 -427 -463 463
		mu 0 3 314 313 308
		f 3 -428 -464 464
		mu 0 3 315 314 308
		f 3 -429 -465 465
		mu 0 3 316 315 308
		f 3 -430 -466 466
		mu 0 3 317 316 308
		f 3 -431 -467 467
		mu 0 3 318 317 308
		f 3 -432 -468 456
		mu 0 3 307 318 308
		f 3 500 501 -503
		mu 0 3 319 320 321
		f 3 524 525 -502
		mu 0 3 320 322 321
		f 3 547 548 -526
		mu 0 3 322 323 321
		f 3 570 571 -549
		mu 0 3 323 324 321
		f 3 593 594 -572
		mu 0 3 324 325 321
		f 3 616 617 -595
		mu 0 3 325 326 321
		f 3 639 640 -618
		mu 0 3 326 327 321
		f 3 662 663 -641
		mu 0 3 327 328 321
		f 3 685 686 -664
		mu 0 3 328 329 321
		f 3 708 709 -687
		mu 0 3 329 330 321
		f 3 731 732 -710
		mu 0 3 330 331 321
		f 3 743 502 -733
		mu 0 3 331 319 321
		f 4 432 479 -491 -469
		mu 0 4 332 333 334 335
		f 4 490 480 -492 -470
		mu 0 4 335 334 336 337
		f 4 491 481 -493 -471
		mu 0 4 337 336 338 339
		f 4 492 482 -494 -472
		mu 0 4 339 338 340 341
		f 4 493 483 -495 -473
		mu 0 4 341 340 342 343
		f 4 494 484 -496 -474
		mu 0 4 343 342 344 345
		f 4 495 485 -497 -475
		mu 0 4 345 344 346 347
		f 4 496 486 -498 -476
		mu 0 4 347 346 348 349
		f 4 497 487 -499 -477
		mu 0 4 349 348 350 351
		f 4 498 488 -500 -478
		mu 0 4 351 350 352 353
		f 4 499 489 -501 -479
		mu 0 4 353 352 320 319
		f 4 433 503 -515 -480
		mu 0 4 333 354 355 334
		f 4 514 504 -516 -481
		mu 0 4 334 355 356 336
		f 4 515 505 -517 -482
		mu 0 4 336 356 357 338
		f 4 516 506 -518 -483
		mu 0 4 338 357 358 340
		f 4 517 507 -519 -484
		mu 0 4 340 358 359 342
		f 4 518 508 -520 -485
		mu 0 4 342 359 360 344
		f 4 519 509 -521 -486
		mu 0 4 344 360 361 346
		f 4 520 510 -522 -487
		mu 0 4 346 361 362 348
		f 4 521 511 -523 -488
		mu 0 4 348 362 363 350
		f 4 522 512 -524 -489
		mu 0 4 350 363 364 352
		f 4 523 513 -525 -490
		mu 0 4 352 364 322 320
		f 4 434 526 -538 -504
		mu 0 4 354 365 366 355
		f 4 537 527 -539 -505
		mu 0 4 355 366 367 356
		f 4 538 528 -540 -506
		mu 0 4 356 367 368 357
		f 4 539 529 -541 -507
		mu 0 4 357 368 369 358
		f 4 540 530 -542 -508
		mu 0 4 358 369 370 359
		f 4 541 531 -543 -509
		mu 0 4 359 370 371 360
		f 4 542 532 -544 -510
		mu 0 4 360 371 372 361
		f 4 543 533 -545 -511
		mu 0 4 361 372 373 362
		f 4 544 534 -546 -512
		mu 0 4 362 373 374 363
		f 4 545 535 -547 -513
		mu 0 4 363 374 375 364
		f 4 546 536 -548 -514
		mu 0 4 364 375 323 322
		f 4 435 549 -561 -527
		mu 0 4 365 376 377 366
		f 4 560 550 -562 -528
		mu 0 4 366 377 378 367
		f 4 561 551 -563 -529
		mu 0 4 367 378 379 368
		f 4 562 552 -564 -530
		mu 0 4 368 379 380 369
		f 4 563 553 -565 -531
		mu 0 4 369 380 381 370
		f 4 564 554 -566 -532
		mu 0 4 370 381 382 371
		f 4 565 555 -567 -533
		mu 0 4 371 382 383 372
		f 4 566 556 -568 -534
		mu 0 4 372 383 384 373
		f 4 567 557 -569 -535
		mu 0 4 373 384 385 374
		f 4 568 558 -570 -536
		mu 0 4 374 385 386 375
		f 4 569 559 -571 -537
		mu 0 4 375 386 324 323
		f 4 436 572 -584 -550
		mu 0 4 376 387 388 377
		f 4 583 573 -585 -551
		mu 0 4 377 388 389 378
		f 4 584 574 -586 -552
		mu 0 4 378 389 390 379
		f 4 585 575 -587 -553
		mu 0 4 379 390 391 380
		f 4 586 576 -588 -554
		mu 0 4 380 391 392 381
		f 4 587 577 -589 -555
		mu 0 4 381 392 393 382
		f 4 588 578 -590 -556
		mu 0 4 382 393 394 383
		f 4 589 579 -591 -557
		mu 0 4 383 394 395 384
		f 4 590 580 -592 -558
		mu 0 4 384 395 396 385
		f 4 591 581 -593 -559
		mu 0 4 385 396 397 386
		f 4 592 582 -594 -560
		mu 0 4 386 397 325 324
		f 4 437 595 -607 -573
		mu 0 4 387 398 399 388
		f 4 606 596 -608 -574
		mu 0 4 388 399 400 389
		f 4 607 597 -609 -575
		mu 0 4 389 400 401 390
		f 4 608 598 -610 -576
		mu 0 4 390 401 402 391
		f 4 609 599 -611 -577
		mu 0 4 391 402 403 392
		f 4 610 600 -612 -578
		mu 0 4 392 403 404 393
		f 4 611 601 -613 -579
		mu 0 4 393 404 405 394
		f 4 612 602 -614 -580
		mu 0 4 394 405 406 395
		f 4 613 603 -615 -581
		mu 0 4 395 406 407 396
		f 4 614 604 -616 -582
		mu 0 4 396 407 408 397
		f 4 615 605 -617 -583
		mu 0 4 397 408 326 325
		f 4 438 618 -630 -596
		mu 0 4 398 409 410 399
		f 4 629 619 -631 -597
		mu 0 4 399 410 411 400
		f 4 630 620 -632 -598
		mu 0 4 400 411 412 401
		f 4 631 621 -633 -599
		mu 0 4 401 412 413 402
		f 4 632 622 -634 -600
		mu 0 4 402 413 414 403
		f 4 633 623 -635 -601
		mu 0 4 403 414 415 404
		f 4 634 624 -636 -602
		mu 0 4 404 415 416 405
		f 4 635 625 -637 -603
		mu 0 4 405 416 417 406
		f 4 636 626 -638 -604
		mu 0 4 406 417 418 407
		f 4 637 627 -639 -605
		mu 0 4 407 418 419 408
		f 4 638 628 -640 -606
		mu 0 4 408 419 327 326
		f 4 439 641 -653 -619
		mu 0 4 409 420 421 410
		f 4 652 642 -654 -620
		mu 0 4 410 421 422 411
		f 4 653 643 -655 -621
		mu 0 4 411 422 423 412
		f 4 654 644 -656 -622
		mu 0 4 412 423 424 413
		f 4 655 645 -657 -623
		mu 0 4 413 424 425 414
		f 4 656 646 -658 -624
		mu 0 4 414 425 426 415
		f 4 657 647 -659 -625
		mu 0 4 415 426 427 416
		f 4 658 648 -660 -626
		mu 0 4 416 427 428 417
		f 4 659 649 -661 -627
		mu 0 4 417 428 429 418
		f 4 660 650 -662 -628
		mu 0 4 418 429 430 419
		f 4 661 651 -663 -629
		mu 0 4 419 430 328 327
		f 4 440 664 -676 -642
		mu 0 4 420 431 432 421
		f 4 675 665 -677 -643
		mu 0 4 421 432 433 422
		f 4 676 666 -678 -644
		mu 0 4 422 433 434 423
		f 4 677 667 -679 -645
		mu 0 4 423 434 435 424
		f 4 678 668 -680 -646
		mu 0 4 424 435 436 425
		f 4 679 669 -681 -647
		mu 0 4 425 436 437 426
		f 4 680 670 -682 -648
		mu 0 4 426 437 438 427
		f 4 681 671 -683 -649
		mu 0 4 427 438 439 428
		f 4 682 672 -684 -650
		mu 0 4 428 439 440 429
		f 4 683 673 -685 -651
		mu 0 4 429 440 441 430
		f 4 684 674 -686 -652
		mu 0 4 430 441 329 328
		f 4 441 687 -699 -665
		mu 0 4 431 442 443 432
		f 4 698 688 -700 -666
		mu 0 4 432 443 444 433
		f 4 699 689 -701 -667
		mu 0 4 433 444 445 434
		f 4 700 690 -702 -668
		mu 0 4 434 445 446 435
		f 4 701 691 -703 -669
		mu 0 4 435 446 447 436
		f 4 702 692 -704 -670
		mu 0 4 436 447 448 437
		f 4 703 693 -705 -671
		mu 0 4 437 448 449 438
		f 4 704 694 -706 -672
		mu 0 4 438 449 450 439
		f 4 705 695 -707 -673
		mu 0 4 439 450 451 440
		f 4 706 696 -708 -674
		mu 0 4 440 451 452 441
		f 4 707 697 -709 -675
		mu 0 4 441 452 330 329
		f 4 442 710 -722 -688
		mu 0 4 442 453 454 443
		f 4 721 711 -723 -689
		mu 0 4 443 454 455 444
		f 4 722 712 -724 -690
		mu 0 4 444 455 456 445
		f 4 723 713 -725 -691
		mu 0 4 445 456 457 446
		f 4 724 714 -726 -692
		mu 0 4 446 457 458 447
		f 4 725 715 -727 -693
		mu 0 4 447 458 459 448
		f 4 726 716 -728 -694
		mu 0 4 448 459 460 449
		f 4 727 717 -729 -695
		mu 0 4 449 460 461 450
		f 4 728 718 -730 -696
		mu 0 4 450 461 462 451
		f 4 729 719 -731 -697
		mu 0 4 451 462 463 452
		f 4 730 720 -732 -698
		mu 0 4 452 463 331 330
		f 4 443 468 -734 -711
		mu 0 4 453 332 335 454
		f 4 733 469 -735 -712
		mu 0 4 454 335 337 455
		f 4 734 470 -736 -713
		mu 0 4 455 337 339 456
		f 4 735 471 -737 -714
		mu 0 4 456 339 341 457
		f 4 736 472 -738 -715
		mu 0 4 457 341 343 458
		f 4 737 473 -739 -716
		mu 0 4 458 343 345 459
		f 4 738 474 -740 -717
		mu 0 4 459 345 347 460
		f 4 739 475 -741 -718
		mu 0 4 460 347 349 461
		f 4 740 476 -742 -719
		mu 0 4 461 349 351 462
		f 4 741 477 -743 -720
		mu 0 4 462 351 353 463
		f 4 742 478 -744 -721
		mu 0 4 463 353 319 331
		f 4 -748 -750 -751 748
		mu 0 4 464 465 466 467
		f 4 750 -747 -746 744
		mu 0 4 467 466 468 469
		f 4 -765 -753 -811 751
		mu 0 4 470 471 472 473
		f 4 810 -756 774 756
		mu 0 4 473 472 474 475
		f 4 -752 -757 757 -767
		mu 0 4 476 477 478 479
		f 4 -755 -812 753 758
		mu 0 4 480 481 482 483
		f 4 755 752 -763 760
		mu 0 4 484 485 486 487
		f 4 -770 767 762 761
		mu 0 4 488 489 487 486
		f 4 -772 -762 764 763
		mu 0 4 490 491 471 470
		f 4 -774 -764 766 765
		mu 0 4 492 493 476 479
		f 4 769 768 754 759
		mu 0 4 489 488 494 495
		f 4 811 -769 771 770
		mu 0 4 482 481 491 490
		f 4 -771 773 772 -754
		mu 0 4 496 493 492 497
		f 4 783 779 -775 -779
		mu 0 4 498 499 500 501
		f 4 -776 -758 -780 780
		mu 0 4 502 503 504 505
		f 4 -777 -778 775 781
		mu 0 4 506 507 508 509
		f 4 -761 776 782 778
		mu 0 4 510 511 512 513
		f 4 814 785 794 -785
		mu 0 4 514 515 516 517
		f 4 788 -815 -790 -784
		mu 0 4 518 515 514 519
		f 4 796 -781 789 784
		mu 0 4 520 521 522 523
		f 4 813 787 -813 -787
		mu 0 4 524 525 526 527
		f 4 -783 792 -786 -789
		mu 0 4 528 529 530 531
		f 4 -792 -793 -798 799
		mu 0 4 532 530 529 533
		f 4 -795 791 801 -794
		mu 0 4 517 516 534 535
		f 4 -796 -797 793 803
		mu 0 4 536 521 520 537
		f 4 -791 -788 -799 -800
		mu 0 4 533 538 539 532
		f 4 -802 798 -814 -801
		mu 0 4 535 534 525 524
		f 4 786 -803 -804 800
		mu 0 4 540 541 536 537
		f 4 -773 -809 802 -805
		mu 0 4 542 543 544 545
		f 4 804 809 -806 -807
		mu 0 4 546 547 548 549
		f 4 807 -760 805 790
		mu 0 4 550 551 552 553
		f 4 777 -768 -816 -766
		mu 0 4 554 555 556 557
		f 4 -817 797 -782 795
		mu 0 4 558 559 560 561
		f 4 816 808 815 -808
		mu 0 4 562 563 564 565
		f 4 -821 -823 -824 821
		mu 0 4 566 567 568 569
		f 4 823 -820 -819 817
		mu 0 4 569 568 570 571
		f 4 -829 830 829 827
		mu 0 4 572 573 574 575
		f 4 -825 825 826 -831
		mu 0 4 573 576 577 574
		f 4 -846 848 860 -836
		mu 0 4 578 579 580 581
		f 4 843 834 852 851
		mu 0 4 582 583 584 585
		f 4 840 -852 854 853
		mu 0 4 586 582 585 587
		f 4 -849 -842 -856 858
		mu 0 4 580 579 588 589
		f 4 -855 -845 842 838
		mu 0 4 587 585 590 591
		f 4 -858 -859 -840 -848
		mu 0 4 592 580 589 593
		f 4 -857 -839 861 839
		mu 0 4 594 595 596 597
		f 4 -834 -854 856 855
		mu 0 4 598 599 595 594
		f 4 -847 -841 833 841
		mu 0 4 600 601 599 598
		f 4 -832 -844 846 845
		mu 0 4 602 603 601 600
		f 4 862 -835 831 835
		mu 0 4 581 584 583 578
		f 4 -833 -851 -863 859
		mu 0 4 604 605 584 581
		f 4 -843 -850 847 -862
		mu 0 4 596 606 607 597
		f 4 -853 863 865 -865
		mu 0 4 585 584 608 609
		f 4 850 866 -868 -864
		mu 0 4 584 605 610 608
		f 4 844 864 -871 -869
		mu 0 4 590 585 609 611
		f 4 -861 871 873 -873
		mu 0 4 581 580 612 613
		f 4 857 874 -876 -872
		mu 0 4 580 592 614 612
		f 4 -860 872 877 -877
		mu 0 4 604 581 613 615
		f 4 -838 876 878 -875
		mu 0 4 592 604 615 614
		f 4 -837 832 837 849
		mu 0 4 606 605 604 607
		f 4 -870 -867 836 868
		mu 0 4 611 610 605 590
		f 4 879 -879 880 869
		mu 0 4 616 617 618 619
		f 4 -878 881 867 -881
		mu 0 4 620 621 622 623
		f 4 -874 882 -866 -882
		mu 0 4 624 625 626 627
		f 4 875 -880 870 -883
		mu 0 4 628 629 630 631;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
parent -s -nc -r -add "|wall|wall4|wallShape2" "wall6" ;
parent -s -nc -r -add "|wall|wall4|wallShape2" "wall8" ;
parent -s -nc -r -add "|wall|wall4|wallShape2" "wall2" ;
parent -s -nc -r -add "|wall|wall4|wallShape2" "wall18" ;
parent -s -nc -r -add "|wall|wall4|wallShape2" "wall20" ;
parent -s -nc -r -add "|wall|wall4|wallShape2" "wall22" ;
parent -s -nc -r -add "|wall|wall4|wallShape2" "wall24" ;
parent -s -nc -r -add "|wall|wall9|wallShape9" "all11" ;
parent -s -nc -r -add "|wall|wall9|wallShape9" "wall13" ;
parent -s -nc -r -add "|wall|wall9|wallShape9" "wall25" ;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 7 ".lnk";
	setAttr -s 7 ".slnk";
createNode displayLayerManager -n "layerManager";
	setAttr ".cdl" 1;
	setAttr -s 5 ".dli[1:4]"  1 2 3 4;
	setAttr -s 4 ".dli";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode script -n "room2:sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode displayLayer -n "room2:ceiling";
	setAttr ".do" 1;
createNode displayLayer -n "room2:floor";
	setAttr ".do" 2;
createNode displayLayer -n "room2:wall";
	setAttr ".do" 4;
createNode blinn -n "room2:blinn2";
	setAttr ".c" -type "float3" 0.458 0.5 0.49292159 ;
	setAttr ".it" -type "float3" 0.17949188 0.17949188 0.17949188 ;
createNode shadingEngine -n "room2:blinn2SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "room2:materialInfo2";
createNode polyNormal -n "room2:polyNormal2";
	setAttr ".ics" -type "componentList" 1 "f[0:3]";
	setAttr ".nm" 2;
	setAttr ".unm" no;
createNode polyNormal -n "room2:polyNormal3";
	setAttr ".ics" -type "componentList" 1 "f[0:3]";
	setAttr ".unm" no;
createNode script -n "room2:uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n"
		+ "                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n"
		+ "                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n"
		+ "            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n"
		+ "                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n"
		+ "                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n"
		+ "            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n"
		+ "                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n"
		+ "                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n"
		+ "                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n"
		+ "            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n"
		+ "            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n"
		+ "        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 1\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 1\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n"
		+ "                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n"
		+ "                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n"
		+ "                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n"
		+ "            -twoSidedLighting 1\n            -backfaceCulling 1\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n"
		+ "            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n"
		+ "            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n"
		+ "                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n"
		+ "            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n"
		+ "            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n"
		+ "                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n"
		+ "                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n"
		+ "                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n"
		+ "                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n"
		+ "                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n"
		+ "                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n"
		+ "                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n"
		+ "            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 19 100 -ps 2 81 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 1\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 1\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode blinn -n "room2:blinn3";
createNode shadingEngine -n "room2:blinn3SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "room2:materialInfo5";
createNode groupId -n "room2:groupId8";
	setAttr ".ihi" 0;
createNode polyDelEdge -n "polyDelEdge3";
	setAttr ".ics" -type "componentList" 1 "e[830]";
	setAttr ".cv" yes;
createNode groupId -n "groupId89";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[0:421]" "f[426:451]";
createNode groupId -n "groupId90";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[422:425]";
createNode polyDelEdge -n "polyDelEdge4";
	setAttr ".ics" -type "componentList" 1 "e[823]";
	setAttr ".cv" yes;
createNode polyTweak -n "polyTweak2";
	setAttr ".uopa" yes;
	setAttr -s 7 ".tk";
	setAttr ".tk[402]" -type "float3" 26.626999 0 0 ;
	setAttr ".tk[403]" -type "float3" 26.626999 0 0 ;
	setAttr ".tk[404]" -type "float3" 26.626999 0 0 ;
	setAttr ".tk[405]" -type "float3" 26.626999 0 0 ;
createNode deleteComponent -n "deleteComponent2";
	setAttr ".dc" -type "componentList" 1 "f[388:389]";
createNode blinn -n "blinn1";
createNode shadingEngine -n "blinn1SG";
	setAttr ".ihi" 0;
	setAttr -s 33 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -s 5 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "room2:floor.di" "ground.do";
connectAttr "room2:ceiling.di" "ceiling.do";
connectAttr "room2:polyNormal3.out" "ceilingShape.i";
connectAttr "room2:wall.di" "wall4.do";
connectAttr "room2:wall.di" "wall9.do";
connectAttr "room2:wall.di" "wall6.do";
connectAttr "room2:wall.di" "wall8.do";
connectAttr "room2:wall.di" "wall2.do";
connectAttr "room2:wall.di" "all11.do";
connectAttr "room2:wall.di" "wall13.do";
connectAttr "room2:wall.di" "wall18.do";
connectAttr "room2:wall.di" "wall20.do";
connectAttr "room2:wall.di" "wall22.do";
connectAttr "room2:wall.di" "wall24.do";
connectAttr "room2:wall.di" "wall25.do";
connectAttr "room2:wall.di" "wall26.do";
connectAttr "room2:wall.di" "wall27.do";
connectAttr "room2:wall.di" "wall7.do";
connectAttr "room2:wall.di" "wall1.do";
connectAttr "room2:wall.di" "wall3.do";
connectAttr "room2:wall.di" "wall5.do";
connectAttr "room2:wall.di" "wall10.do";
connectAttr "room2:wall.di" "wall12.do";
connectAttr "room2:wall.di" "wall14.do";
connectAttr "room2:wall.di" "wall16.do";
connectAttr "room2:wall.di" "wall17.do";
connectAttr "room2:wall.di" "wall19.do";
connectAttr "room2:wall.di" "wall21.do";
connectAttr "room2:wall.di" "wall23.do";
connectAttr "room2:wall.di" "wall29.do";
connectAttr "room2:wall.di" "wall34.do";
connectAttr "room2:wall.di" "wall30.do";
connectAttr "room2:wall.di" "wall31.do";
connectAttr "room2:wall.di" "wall32.do";
connectAttr "room2:wall.di" "wall33.do";
connectAttr "deleteComponent2.og" "doorShape1.i";
connectAttr "groupId89.id" "doorShape1.iog.og[0].gid";
connectAttr "room2:blinn3SG.mwc" "doorShape1.iog.og[0].gco";
connectAttr "groupId90.id" "doorShape1.iog.og[1].gid";
connectAttr "room2:blinn2SG.mwc" "doorShape1.iog.og[1].gco";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "room2:blinn2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "room2:blinn3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "room2:blinn2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "room2:blinn3SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "layerManager.dli[1]" "room2:ceiling.id";
connectAttr "layerManager.dli[2]" "room2:floor.id";
connectAttr "layerManager.dli[4]" "room2:wall.id";
connectAttr "room2:blinn2.oc" "room2:blinn2SG.ss";
connectAttr "doorShape1.iog.og[1]" "room2:blinn2SG.dsm" -na;
connectAttr "groupId90.msg" "room2:blinn2SG.gn" -na;
connectAttr "room2:blinn2SG.msg" "room2:materialInfo2.sg";
connectAttr "room2:blinn2.msg" "room2:materialInfo2.m";
connectAttr "room2:polySurfaceShape19.o" "room2:polyNormal2.ip";
connectAttr "room2:polyNormal2.out" "room2:polyNormal3.ip";
connectAttr "room2:blinn3.oc" "room2:blinn3SG.ss";
connectAttr "doorShape1.iog.og[0]" "room2:blinn3SG.dsm" -na;
connectAttr "groupId89.msg" "room2:blinn3SG.gn" -na;
connectAttr "room2:blinn3SG.msg" "room2:materialInfo5.sg";
connectAttr "room2:blinn3.msg" "room2:materialInfo5.m";
connectAttr "groupParts4.og" "polyDelEdge3.ip";
connectAttr "polySurfaceShape2.o" "groupParts3.ig";
connectAttr "groupId89.id" "groupParts3.gi";
connectAttr "groupParts3.og" "groupParts4.ig";
connectAttr "groupId90.id" "groupParts4.gi";
connectAttr "polyDelEdge3.out" "polyDelEdge4.ip";
connectAttr "polyDelEdge4.out" "polyTweak2.ip";
connectAttr "polyTweak2.out" "deleteComponent2.ig";
connectAttr "blinn1.oc" "blinn1SG.ss";
connectAttr "groundShape.iog" "blinn1SG.dsm" -na;
connectAttr "wallShape15.iog" "blinn1SG.dsm" -na;
connectAttr "wallShape34.iog" "blinn1SG.dsm" -na;
connectAttr "wallShape29.iog" "blinn1SG.dsm" -na;
connectAttr "wallShape23.iog" "blinn1SG.dsm" -na;
connectAttr "wallShape21.iog" "blinn1SG.dsm" -na;
connectAttr "wallShape19.iog" "blinn1SG.dsm" -na;
connectAttr "wallShape17.iog" "blinn1SG.dsm" -na;
connectAttr "wallShape16.iog" "blinn1SG.dsm" -na;
connectAttr "wallShape14.iog" "blinn1SG.dsm" -na;
connectAttr "wallShape12.iog" "blinn1SG.dsm" -na;
connectAttr "wallShape10.iog" "blinn1SG.dsm" -na;
connectAttr "wallShape5.iog" "blinn1SG.dsm" -na;
connectAttr "wallShape3.iog" "blinn1SG.dsm" -na;
connectAttr "wallShape1.iog" "blinn1SG.dsm" -na;
connectAttr "wallShape7.iog" "blinn1SG.dsm" -na;
connectAttr "wallShape27.iog" "blinn1SG.dsm" -na;
connectAttr "wallShape26.iog" "blinn1SG.dsm" -na;
connectAttr "|wall|wall25|wallShape9.iog" "blinn1SG.dsm" -na;
connectAttr "|wall|wall24|wallShape2.iog" "blinn1SG.dsm" -na;
connectAttr "|wall|wall22|wallShape2.iog" "blinn1SG.dsm" -na;
connectAttr "|wall|wall20|wallShape2.iog" "blinn1SG.dsm" -na;
connectAttr "|wall|wall18|wallShape2.iog" "blinn1SG.dsm" -na;
connectAttr "|wall|wall13|wallShape9.iog" "blinn1SG.dsm" -na;
connectAttr "|wall|all11|wallShape9.iog" "blinn1SG.dsm" -na;
connectAttr "|wall|wall2|wallShape2.iog" "blinn1SG.dsm" -na;
connectAttr "|wall|wall8|wallShape2.iog" "blinn1SG.dsm" -na;
connectAttr "|wall|wall6|wallShape2.iog" "blinn1SG.dsm" -na;
connectAttr "|wall|wall9|wallShape9.iog" "blinn1SG.dsm" -na;
connectAttr "|wall|wall4|wallShape2.iog" "blinn1SG.dsm" -na;
connectAttr "wallShape31.iog" "blinn1SG.dsm" -na;
connectAttr "wallShape32.iog" "blinn1SG.dsm" -na;
connectAttr "wallShape33.iog" "blinn1SG.dsm" -na;
connectAttr "blinn1SG.msg" "materialInfo1.sg";
connectAttr "blinn1.msg" "materialInfo1.m";
connectAttr "room2:blinn2SG.pa" ":renderPartition.st" -na;
connectAttr "room2:blinn3SG.pa" ":renderPartition.st" -na;
connectAttr "blinn1SG.pa" ":renderPartition.st" -na;
connectAttr "ceilingShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "room2:groupId8.msg" ":initialShadingGroup.gn" -na;
connectAttr "room2:blinn2.msg" ":defaultShaderList1.s" -na;
connectAttr "room2:blinn3.msg" ":defaultShaderList1.s" -na;
connectAttr "blinn1.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
dataStructure -fmt "raw" -as "name=externalContentTable:string=node:string=key:string=upath:uint32=upathcrc:string=rpath:string=roles";
applyMetadata -fmt "raw" -v "channel\nname externalContentTable\nstream\nname v1.0\nindexType numeric\nstructure externalContentTable\n0\n\"file1\" \"fileTextureName\" \"C:/Users/Game07/Desktop/Haley Zhu/1/UV_1k.jpg\" 2622073563 \"C:/Users/Game07/Desktop/Haley Zhu/1/UV_1k.jpg\" \"sourceImages\"\n1\n\"file2\" \"fileTextureName\" \"C:/Users/Game07/Desktop/Haley Zhu/2/ctrl_z/tex/wall.tga\" 3440200458 \"C:/Users/Game07/Desktop/Haley Zhu/2/ctrl_z/tex/wall.tga\" \"sourceImages\"\nendStream\nendChannel\nendAssociations\n" 
		-scn;
// End of StandardRoom1.ma
